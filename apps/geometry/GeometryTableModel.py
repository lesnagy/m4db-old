from PyQt5 import QtCore

from PyQt5.QtCore import QAbstractTableModel
from PyQt5.QtCore import QVariant

from preferences import config

from log import logger

import rest.client.geometry

class GeometryTableModel(QAbstractTableModel):


    def __init__(self, parent=None):
        super(GeometryTableModel, self).__init__(parent=parent)

        self.table_data = rest.client.geometry.geometry_collection()

        logger.debug("Constructor complete")


    def rowCount(self, parent):
        if not self.table_data:
            return 1

        return len(self.table_data)


    def columnCount(self, parent):
        return 8


    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "name"
                if section == 1:
                    return "size"
                elif section == 2:
                    return "description"
                elif section == 3:
                    return "volume"
                elif section == 4:
                    return "elements"
                elif section == 5:
                    return "vertices"
                elif section == 6:
                    return "size unit"
                elif section == 7:
                    return "intrinsic unit"
            elif orientation == QtCore.Qt.Vertical:
                return "{n:05d}".format(n=section)
        return QVariant()


    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            if not self.table_data:
                return ""
            else:
                if index.column() == 0:
                    return self.table_data[index.row()]['name']
                if index.column() == 1:
                    return self.table_data[index.row()]['size']
                elif index.column() == 2:
                    return self.table_data[index.row()]['description']
                elif index.column() == 3:
                    return self.table_data[index.row()]['volume_total']
                elif index.column() == 4:
                    return self.table_data[index.row()]['nelements']
                elif index.column() == 5:
                    return self.table_data[index.row()]['nvertices']
                elif index.column() == 6:
                    return self.table_data[index.row()]['size_unit']
                elif index.column() == 7:
                    return self.table_data[index.row()]['intrinsic_unit']


