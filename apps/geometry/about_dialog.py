r'''
The main window object. This object:
    * loads the layout from 'main_window.ui', 
    * defines bespoke user interface elements,
    * sets up user interface signals/slots/connections.
'''

import vtk

from PyQt5 import QtCore, QtGui, Qt, QtWidgets
from PyQt5.QtWidgets import qApp

from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from apps.geometry.ui_about_dialog import Ui_AboutDialog

from log import logger

from apps.preferences.preferences_dialog import PreferencesDialog


class AboutDialog(Qt.QMainWindow):

    def __init__(self, parent=None):
        Qt.QMainWindow.__init__(self, parent)

        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)






























        # Set up the interface from designer
        
#        # Matplotlib bespoke UI elements
#        mpl_test_canvas = MplTestCanvas(self, width=5, height=4, dpi=100)
#        self.ui.graph_layout.addWidget(mpl_test_canvas)
#
#        # VTK bespoke UI elements
#        self.vtk_widget = QVTKRenderWindowInteractor(self)
#        self.ui.vtk_layout.addWidget(self.vtk_widget)
#
#        self.ren = vtk.vtkRenderer()
#        self.vtk_widget.GetRenderWindow().AddRenderer(self.ren)
#        self.iren = self.vtk_widget.GetRenderWindow().GetInteractor()
#
#        ##### Create source
#        source = vtk.vtkSphereSource()
#        source.SetCenter(0, 0, 0)
#        source.SetRadius(5.0)
#
#        ##### Create a mapper
#        mapper = vtk.vtkPolyDataMapper()
#        mapper.SetInputConnection(source.GetOutputPort())
#
#        ##### Create an actor
#        actor = vtk.vtkActor()
#        actor.SetMapper(mapper)
#
#        self.ren.AddActor(actor)
#
#        self.ren.ResetCamera()
#
#        #self.frame.setLayout(self.vl)
#        #self.setCentralWidget(self.frame)
#
#        self.show()
#        self.iren.Initialize()
#        self.iren.Start()
#
#
#
#        # Connections

