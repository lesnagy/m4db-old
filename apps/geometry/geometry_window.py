r'''
The main window object. This object:
    * loads the layout from 'main_window.ui', 
    * defines bespoke user interface elements,
    * sets up user interface signals/slots/connections.
'''
import sys
import os

import vtk

from PyQt5 import QtCore, QtGui, Qt, QtWidgets
from PyQt5.QtWidgets import qApp

from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from apps.geometry.ui_geometry_window import Ui_GeometryWindow

from apps.preferences.preferences_dialog import PreferencesDialog

from log import logger

from apps.geometry.about_dialog import AboutDialog
from apps.geometry.GeometryTableModel import *

import rest.client.geometry

import meshio.patran
import meshio.visualization

from preferences import config_file_apath

class GeometryWindow(Qt.QMainWindow):

    def __init__(self, parent=None):
        Qt.QMainWindow.__init__(self, parent)

        self.ui = Ui_GeometryWindow()
        self.ui.setupUi(self)

        # Table
        self.geometry_table_model = GeometryTableModel(self)
        self.ui.geometry_table.setModel(self.geometry_table_model)
        self.ui.geometry_table.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectRows
        )

        # Table, v_popup
        v_heads = self.ui.geometry_table.verticalHeader()
        v_heads.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        v_heads.customContextMenuRequested.connect(self.geometry_table_popup)

        # Menu
        self.ui.actn_quit.triggered.connect(qApp.quit)
        self.ui.actn_preferences.triggered.connect(self.preferences)
        self.ui.actn_about.triggered.connect(self.about)

        # Controls options
        self.show_grid = False
        self.show_axes = False

        self.ui.check_show_grid.stateChanged.connect(self.check_show_grid_changed)
        self.ui.check_show_axes.stateChanged.connect(self.check_show_axes_changed)

        # VTK
        self.vtk_geometry = QVTKRenderWindowInteractor(self.ui.frm_geometry)
        self.layout_geometry = QtWidgets.QVBoxLayout()
        self.ui.frm_geometry.setLayout(self.layout_geometry)
        self.layout_geometry.addWidget(self.vtk_geometry)

        self.renderer = vtk.vtkRenderer()
        self.vtk_geometry.GetRenderWindow().AddRenderer(self.renderer)
        self.iren = self.vtk_geometry.GetRenderWindow().GetInteractor()
        self.trackball = vtk.vtkInteractorStyleTrackballCamera()
        self.iren.SetInteractorStyle(self.trackball)

        actor = vtk.vtkTextActor()
        actor.SetInput("view empty")
        actor.GetTextProperty().SetFontSize(24)

        self.renderer.AddActor(actor)

        self.renderer.ResetCamera()

        self.show()
        self.iren.Initialize()
        self.iren.Start()


    def preferences(self):
        dialog = PreferencesDialog(self)
        dialog.show()


    def about(self):
        dialog = AboutDialog(self)
        dialog.show()


    def geometry_table_popup(self, pos):
        menu = QtWidgets.QMenu(self)

        view_actn = QtWidgets.QAction("view", self)
        view_actn.triggered.connect(self.geometry_table_popup_view)

        menu.addAction(view_actn)
        
        action = menu.exec_(self.mapToGlobal(pos))


    def geometry_table_popup_view(self):
        selection_model = self.ui.geometry_table.selectionModel()
        if selection_model.hasSelection():
            selection = selection_model.selectedRows()
            if len(selection) >= 1:
                current_selection = selection[0]
                row_idx = current_selection.row()
                unique_id = self.geometry_table_model.table_data[row_idx]['unique_id']
                rest.client.geometry.geometry_patran_mesh(unique_id)
                self.display_mesh(unique_id)


    def display_mesh(self, unique_id):
        cache_dir = os.path.join(
            config_file_apath,
            'geometry',
            unique_id
        )

        cache_mesh = os.path.join(
            cache_dir,
            'mesh.pat'
        )

        patran_data = meshio.patran.read_patran_file(cache_mesh)
        self.patran_mesh = meshio.visualization.MeshGeometry(
            patran_data['nodes'], patran_data['elems']
        )

        self.renderer.RemoveAllViewProps()
        self.renderer.AddActor(self.patran_mesh.getGeometryActor())
        self.vtk_geometry.Render()


    def check_show_grid_changed(self, status):
        logger.debug(status)


    def check_show_axes_changed(self, status):
        logger.debug(status)


        # Set up the interface from designer
        
#        # Matplotlib bespoke UI elements
#        mpl_test_canvas = MplTestCanvas(self, width=5, height=4, dpi=100)
#        self.ui.graph_layout.addWidget(mpl_test_canvas)
#
#        # VTK bespoke UI elements
#        self.vtk_widget = QVTKRenderWindowInteractor(self)
#        self.ui.vtk_layout.addWidget(self.vtk_widget)
#
#        self.ren = vtk.vtkRenderer()
#        self.vtk_widget.GetRenderWindow().AddRenderer(self.ren)
#        self.iren = self.vtk_widget.GetRenderWindow().GetInteractor()
#
#        ##### Create source
#        source = vtk.vtkSphereSource()
#        source.SetCenter(0, 0, 0)
#        source.SetRadius(5.0)
#
#        ##### Create a mapper
#        mapper = vtk.vtkPolyDataMapper()
#        mapper.SetInputConnection(source.GetOutputPort())
#
#        ##### Create an actor
#        actor = vtk.vtkActor()
#        actor.SetMapper(mapper)
#
#        self.ren.AddActor(actor)
#
#        self.ren.ResetCamera()
#
#        #self.frame.setLayout(self.vl)
#        #self.setCentralWidget(self.frame)
#
#        self.show()
#        self.iren.Initialize()
#        self.iren.Start()
#
#
#
#        # Connections

