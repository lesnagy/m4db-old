r'''
The main window object. This object:
    * loads the layout from 'main_window.ui', 
    * defines bespoke user interface elements,
    * sets up user interface signals/slots/connections.
'''

import vtk

from PyQt5 import QtCore, QtGui, Qt, QtWidgets

from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from apps.preferences.ui_preferences_dialog import Ui_PreferencesDialog

from log import logger

import preferences

class PreferencesDialog(Qt.QDialog):

    def __init__(self, parent=None):
        Qt.QDialog.__init__(self, parent)

        self.ui = Ui_PreferencesDialog()
        self.ui.setupUi(self)

        # populate values from current config
        self.populate_cmb_ui_filter_start_point()
        self.populate_cmb_log_level()
        self.populate_txt_log_output()
        self.populate_txt_log_format()
        self.populate_txt_db_db()
        self.populate_txt_db_host()
        self.populate_txt_db_url_template()
        self.populate_txt_db_user()
        self.populate_txt_db_file_root()
        self.populate_cmb_geom_size_convention()
        self.populate_cmb_geom_size_unit()
        self.populate_txt_rest_base_url()
        self.populate_txt_rest_max_tries()
        self.populate_txt_rest_encoding()

        # Connections
        self.ui.cmb_ui_filter_start_point.currentIndexChanged.connect(
            self.change_cmb_ui_filter_start_point
        )
        self.ui.cmb_log_level.currentIndexChanged.connect(
            self.change_cmb_log_level
        )
        self.ui.txt_log_output.textChanged.connect(
            self.change_txt_log_output
        )
        self.ui.txt_log_format.textChanged.connect(
            self.change_txt_log_format
        )
        self.ui.txt_db_db.textChanged.connect(
            self.change_txt_db_db
        )
        self.ui.txt_db_host.textChanged.connect(
            self.change_txt_db_host
        )
        self.ui.txt_db_url_template.textChanged.connect(
            self.change_txt_db_url_template
        )
        self.ui.txt_db_user.textChanged.connect(
            self.change_txt_db_user
        )
        self.ui.txt_db_file_root.textChanged.connect(
            self.change_txt_db_file_root
        )
        self.ui.cmb_geom_size_convention.currentIndexChanged.connect(
            self.change_cmb_geom_size_convention
        )
        self.ui.cmb_geom_size_unit.currentIndexChanged.connect(
            self.change_cmb_geom_size_unit
        )
        self.ui.txt_rest_base_url.textChanged.connect(
            self.change_txt_rest_base_url
        )
        self.ui.txt_rest_encoding.textChanged.connect(
            self.change_txt_rest_encoding
        )

    ###########################################################################
    # Change routines here                                                    #
    ###########################################################################

    def change_cmb_ui_filter_start_point(self, index):
        r'''
        PRIVATE.
        Called when cmb_ui_filter_start_point is changed.

        '''
        preferences.config['general-ui']['filter-start-point'] = \
            self.filter_start_points[index]

        self.update_config()

        logger.debug(
                "set general-ui:filter-start-point to {}".format(
                self.filter_start_points[index]
            )
        )


    def change_cmb_log_level(self, index):
        r'''
        PRIVATE.
        Called when txt_log_level is changed.

        '''
        preferences.config['logging']['level'] = self.log_levels[index]

        self.update_config()

        logger.debug(
            "set logging:level to {}".format(
                self.log_levels[index]
            )
        )


    def change_txt_log_output(self, text):
        r'''
        PRIVATE.
        Called when txt_log_output is changed.

        '''
        preferences.config['logging']['output'] = text

        self.update_config()

        logger.debug(
            "set logging:output to {}".format(text)
        )


    def change_txt_log_format(self, text):
        r'''
        PRIVATE.
        Called when txt_log_format is changed.

        '''
        preferences.config['logging']['format'] = text

        self.update_config()

        logger.debug(
            "set logging:format to {}".format(text)
        )


    def change_txt_db_db(self, text):
        r'''
        PRIVATE.
        Called when txt_db_db is changed.

        '''
        preferences.config['database']['db'] = text

        self.update_config()

        logger.debug(
            'set database:db to {}'.format(text)
        )


    def change_txt_db_host(self, text):
        r'''
        PRIVATE.
        Called when txt_db_host is changed.

        '''
        preferences.config['database']['host'] = text

        self.update_config()

        logger.debug(
            'set database:host to {}'.format(text)
        )


    def change_txt_db_url_template(self, text):
        r'''
        PRIVATE.
        Called when txt_url_template is changed.

        '''
        preferences.config['database']['url-template'] = text

        self.update_config()

        logger.debug(
            'set database:url-template to {}'.format(text)
        )


    def change_txt_db_user(self, text):
        r'''
        PRIVATE.
        Called when txt_db_user is changed.

        '''
        preferences.config['database']['user'] = text

        self.update_config()

        logger.debug(
            'set database:user to {}'.format(text)
        )

        
    def change_txt_db_file_root(self, text):
        r'''
        PRIVATE.
        Called when txt_db_file_root is changed.

        '''
        preferences.config['database']['file-root'] = text

        self.update_config()

        logger.debug(
            'set database:file-root to {}'.format(text)
        )


    def change_cmb_geom_size_convention(self, index):
        r'''
        PRIVATE.
        Called when cmb_geom_size_convention is changed.

        '''
        preferences.config['geometry']['size-convention'] = \
            self.size_conventions[index]

        self.update_config()

        logger.debug(
            'set geometry:size-convention to {}'.format(
                self.size_conventions[index]
            )
        )


    def change_cmb_geom_size_unit(self, index):
        r'''
        PRIVATE.
        Called when cmb_geom_size_unit is changed.

        '''
        preferences.config['geometry']['size-unit'] = \
            self.size_units[index]

        self.update_config()

        logger.debug(
            'set geometry:size-unit to {}'.format(
                self.size_units[index]
            )
        )

    def change_txt_rest_base_url(self, text):
        r'''
        PRIVATE
        Called when txt_rest_base_url is changed.
        '''
        preferences.config['rest']['base-url'] = text

        self.update_config()

        logger.debug(
            "set rest:base-url to {}".format(text)
        )

        
    def change_txt_rest_max_tries(self, text):
        r'''
        PRIVATE
        Called when txt_rest_max_tries is changed.
        '''
        preferences.config['rest']['max-tries'] = text

        self.update_config()

        logger.debug(
            "set rest:max-tries to {}".format(text)
        )


    def change_txt_rest_encoding(self, text):
        r'''
        PRIVATE
        Called when txt_rest_encoding is changed.
        '''
        preferences.config['rest']['encoding'] = text

        self.update_config()

        logger.debug(
            "set rest:encoding to {}".format(text)
        )

    ###########################################################################
    # Populate routines here                                                  #
    ###########################################################################

    def populate_cmb_ui_filter_start_point(self):
        r'''
        PRIVATE.
        Populates the cmb_ui_filter_start_point combo box with data and syncs
        the active value with the on in the .m4db.ini config file.

        '''
        self.filter_start_points = []
        self.filter_start_points.append('geometry')
        self.filter_start_points.append('material')
        self.ui.cmb_ui_filter_start_point.setModel(
            Qt.QStringListModel(self.filter_start_points)
        )

        cfg_val = preferences.config.get('general-ui', 'filter-start-point')
        for i in range(len(self.filter_start_points)):
            cmb_val = self.filter_start_points[i]
            if cmb_val == cfg_val:
                self.ui.cmb_ui_filter_start_point.setCurrentIndex(i)
                break


    def populate_cmb_log_level(self):
        r'''
        PRIVATE.
        Populate/sync the cmb_log_level value with the one in .m4db.ini config
        file.
        
        '''
        self.log_levels = []
        self.log_levels.append('debug')
        self.log_levels.append('info')
        self.log_levels.append('warning')
        self.log_levels.append('error')
        self.log_levels.append('critical')
        self.ui.cmb_log_level.setModel(
            Qt.QStringListModel(self.log_levels)
        )

        cfg_val = preferences.config.get('logging', 'level')
        for i in range(len(self.log_levels)):
            cmb_val = self.log_levels[i]
            if cmb_val == cfg_val:
                self.ui.cmb_log_level.setCurrentIndex(i)
                break


    def populate_txt_log_output(self):
        r'''
        PRIVATE.
        Populate/sync the txt_log_output file name with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_log_output.setText(
            preferences.config.get('logging', 'output')
        )


    def populate_txt_log_format(self):
        r'''
        PRIVATE.
        Populate/sync the txt_log_format with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_log_format.setText(
            preferences.config.get('logging', 'format')
        )


    def populate_txt_db_db(self):
        r'''
        PRIVATE.
        Populate/sync the txt_db_db with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_db_db.setText(
            preferences.config.get('database', 'db')
        )


    def populate_txt_db_host(self):
        r'''
        PRIVATE.
        Populate/sync the txt_db_host with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_db_host.setText(
            preferences.config.get('database', 'host')
        )


    def populate_txt_db_url_template(self):
        r'''
        PRIVATE.
        Populate/sync the txt_db_url_template with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_db_url_template.setText(
            preferences.config.get('database', 'url-template')
        )


    def populate_txt_db_user(self):
        r'''
        PRIVATE.
        Populate/sync the txt_db_user with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_db_user.setText(
            preferences.config.get('database', 'user')
        )


    def populate_txt_db_file_root(self):
        r'''
        PRIVATE.
        Populate/sync the txt_db_file_root with the one in .m4db.ini
        config file
        
        '''
        self.ui.txt_db_file_root.setText(
            preferences.config.get('database', 'file-root')
        )


    def populate_cmb_geom_size_convention(self):
        r'''
        PRIVATE.
        Populate/sync the cmb_geom_size_convention value with the one in
        .m4db.ini config file.
        
        '''
        self.size_conventions = []
        self.size_conventions.append('ESVD')
        self.size_conventions.append('ECVL')
        self.ui.cmb_geom_size_convention.setModel(
            Qt.QStringListModel(self.size_conventions)
        )

        cfg_val = preferences.config.get('geometry', 'size-convention')
        for i in range(len(self.size_conventions)):
            cmb_val = self.size_conventions[i]
            if cmb_val == cfg_val:
                self.ui.cmb_geom_size_convention.setCurrentIndex(i)
                break


    def populate_cmb_geom_size_unit(self):
        r'''
        PRIVATE.
        Populate/sync the cmb_geom_size_unit value with the one in
        .m4db.ini config file.
        
        '''
        self.size_units = []
        self.size_units.append('m')
        self.size_units.append('cm')
        self.size_units.append('mm')
        self.size_units.append('um')
        self.size_units.append('nm')
        self.size_units.append('pm')
        self.size_units.append('fm')
        self.size_units.append('am')
        self.ui.cmb_geom_size_unit.setModel(
            Qt.QStringListModel(self.size_units)
        )

        cfg_val = preferences.config.get('geometry', 'size-unit')
        for i in range(len(self.size_units)):
            cmb_val = self.size_units[i]
            if cmb_val == cfg_val:
                self.ui.cmb_geom_size_unit.setCurrentIndex(i)
                break


    def populate_txt_rest_base_url(self):
        r'''
        PRIVATE.
        Populate/sync the txt_rest_base_url with the one in .m4db.ini
        config file.

        '''
        self.ui.txt_rest_base_url.setText(
            preferences.config.get('rest', 'base-url')
        )


    def populate_txt_rest_max_tries(self):
        r'''
        PRIVATE.
        Populate/sync the txt_rest_max_tries with the one in .m4db.ini
        config file.

        '''
        self.ui.txt_rest_max_tries.setText(
            preferences.config.get('rest', 'max-tries')
        )


    def populate_txt_rest_encoding(self):
        r'''
        PRIVATE.
        Populate/sync the txt_rest_encoding with the one in .m4db.ini
        config file.

        '''
        self.ui.txt_rest_encoding.setText(
            preferences.config.get('rest', 'encoding')
        )


    def update_config(self):
        with open(preferences.config_file_aname, 'w') as fout:
            preferences.config.write(fout)
        
        

