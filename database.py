"""
This module contains a collection of useful database-centric utility functions.

"""
import sys
import re

from sqlalchemy import create_engine
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker

import orm

import preferences

def get_session(create=False, echo=False):
    r'''
    Retrieve a database session object for the given database.

    Args:
        create:    if true the database will be created and then the session
                   will be retrieved.

        echo:      if true sql statements sent to the SQL alchemy ORM will
                   will be echoed to standard output.

    Throws:
        None

    Returns:
        an SQL-alchemy session that may be used to query the database.

    '''
    if not hasattr(get_session, 'engine'):

        db_url = preferences.config.get('database', 'url-template').format(
            user=preferences.config.get('database', 'user'),
            host=preferences.config.get('database', 'host'),
            db=preferences.config.get('database', 'db')
        )

        get_session.engine = create_engine(db_url, echo=echo)
        get_session.engine.connect()

        get_session.session_class = sessionmaker(
            bind=get_session.engine
        )

    if create:
        orm.Base.metadata.create_all(get_session.engine)

    session = get_session.session_class()

    return session


