r"""
Utility functions to retrieve exchange calculation information from the
database.

"""
import orm

from sqlalchemy import text


def get(session,
        id
    ):
    r"""
    Retreive exchange calculation objects from the database.

    Args:
        session: the database session.

        id:      the id of the exchange calculation object.

    Returns:
        an exchange calculation orm object.

    Raises:
        none

    """
    dbo_exchange_calculation = session.query(orm.ExchangeCalculation).\
            filter(orm.ExchangeCalculation.script_id == id).\
            first()

    return dbo_exchange_calculation

