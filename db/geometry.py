r"""
Utilitiy functions to retrieve geometry information from the database.

"""
import orm

from sqlalchemy import text

def get(session,
        name=None,
        size=None,
        size_unit=None,
        size_convention=None,
        distinct=None,
        order_by=None
    ):
    r"""
    Retrieve geometries form the database.

    Args:
        session:         the database session.

        name:            the name of the geometry.

        size:            the size of the geometry.

        size_unit:       the unit of size for the geometry.

        size_convention: the size convention for the geometry.

        distinct:        string indicating which attribute to apply a distinct
                         clause over ('name' or 'size'), should be used; by
                         default this is None indicating no distinct clause.

        order_by:        string indicating which attribute to order by

    Returns:
        A list of geometry orm objects encapsulating the geometries that match
        the input filters.

    Raises:
        None

    """
    # The query
    query = session.query(orm.Geometry).\
            join(orm.Geometry.size_unit).\
            join(orm.Geometry.size_convention)

    # Optional filters
    if name:
        query = query.filter(orm.Geometry.name == name)
    if size:
        query = query.filter(orm.Geometry.size == size)
    if size_unit:
        query = query.filter(orm.Unit.symbol == size_unit)
    if size_convention:
        query = query.filter(orm.SizeConvention.symbol == size_convention)

    # Distinct clause (if required)
    if distinct:
        # List of attributes that we can use to distinguish search filter
        distinct_str_to_attr = {
            'name': orm.Geometry.name,
            'size': orm.Geometry.size
        }

        # Apply the distinct clause
        distinct_clause_applied = False
        for attr in distinct_str_to_attr.keys():
            if distinct == attr:
                query = query.distinct(distinct_str_to_attr[attr])
                distinct_clause_applied = True

        # If the clause was not applied, raise an exception
        if not distinct_clause_applied:
            raise ValueError(
                "Unknown attribute for distinct clause '{}' "\
                "should be one of: '{}'".format(
                    distinct,
                    ", ".join(distinct_str_to_attr.keys())
                )
            )

    # Order by clause (if required)
    if order_by:
        # List of attributes that we can use to order by
        order_by_str_to_attr = {
            'name': orm.Geometry.name,
            'size': orm.Geometry.size
        }

        # apply the order by clause
        order_by_clause_applied = False
        for attr in order_by_str_to_attr.keys():
            if order_by == attr:
                query = query.order_by(order_by_str_to_attr[attr])
                order_by_clause_applied = True

        # If the clause was not applied, raise an exception
        if not order_by_clause_applied:
            raise ValueError(
                "Unknown attribute for order by clause '{}', "\
                "should be one of: '{}'".format(
                    order_by,
                    ", ".join(order_by_str_to_attr.keys())
                )
            )

    dbo_geometries = query.all()

    return dbo_geometries

