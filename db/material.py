r"""
Utility functions to retrieve material information from the database.

"""
import orm

from sqlalchemy import text


def get(session,
        name=None,
        temperature=None,
        limit=None,
        offset=None
    ):
    r"""
    Retreive materials from the database.

    Args:
        session:     the database session.

        name:        the geometry name.

        temperature: an integer value of the temperature at which the geometry
                     data is defined.

        limit:       limits the number of records returned by the databse, by
                     default this is None indicating that all records will be
                     returned.

        offset:      the record offset from which to apply a limit

    Returns:
        A list of material orm objects encapsulating the materials that match
        the input filters.

    Raises:
        None
    """

    query = session.query(orm.Material)

    # Build the query

    if name:
        query = query.filter(orm.Material.name == name)
    if temperature:
        query = query.filter(orm.Material.temperature == temperature)
    if limit:
        query = query.limit(limit)
    if offset:
        query = query.offset(offset)

    # Run the query and retrieve objects

    dbo_materials = query.all()

    return dbo_materials

