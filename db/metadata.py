r"""
Utility functions to retrieve metadata information from the database.

"""
import orm

from sqlalchemy import text

def get(session,
        user_name=None,
        project_name=None,
        software_name=None,
        software_version=None,
        limit=None,
        offset=None
    ):
    r"""
    Retrieve metadata from the database.

    Args:
        session:          the database session.

        user_name:        the name of the user associated with the metadata.

        project_name:     the name of the project associated with the metadata.

        software_name:    the name of the software associated with the metadata.

        software_version: the version of the software associated with the
                          metadata.

    Returns:
        a list of metadata orm objects encapsulating the metadata that match
        input filters.

    Raises:
        none

    """
    if software_name is not None and software_version is not None:
        query = session.query(orm.Metadata).\
                join(orm.Project).\
                join(orm.DBUser).\
                join(orm.Software).\
            filter(orm.Metadata.project_id == orm.Project.id).\
            filter(orm.Metadata.db_user_id == orm.DBUser.id).\
            filter(orm.Metadata.software_id == orm.Software.id)
    else:
        query = session.query(orm.Metadata).\
                join(orm.Project).\
                join(orm.DBUser).\
            filter(orm.Metadata.project_id == orm.Project.id).\
            filter(orm.Metadata.db_user_id == orm.DBUser.id)

    if user_name:
        query = query.filter(orm.DBUser.user_name == user_name)
    if software_name:
        query = query.filter(orm.Software.name == software_name)
    if software_version:
        query = query.filter(orm.Software.version == software_version)
    if project_name:
        query = query.filter(orm.Project.name == project_name)

    dbo_metadatas = query.all()

    return dbo_metadatas
