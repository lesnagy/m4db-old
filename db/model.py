r"""
Utility functions to retrieve model information from the database.

"""
import orm

from sqlalchemy import text

import preferences

def get_by_unique_id(session, unique_id):
    r"""
    Retrieve the model with the given unique_id.

    Args:
        session:   the database session.

        unique_id: the unique identifier for the model.

    Returs:
        the orm object encapsulating the model that matches unique_id.

    Raises:
        ValueError if more than one model with the given unique_id is found
        (this should *never* happen).

    """
    dbo_models = session.query(orm.Model).\
                    filter(orm.Model.unique_id == unique_id).\
                    all()

    if len(dbo_models) > 1:
        raise ValueError("*FATAL* more than one model has a unique id!")
    elif len(dbo_models) == 0:
        return None

    return dbo_models[0]


def get(session,
        geometry=None,
        size=None,
        size_unit=preferences.config.get('data-defaults', 'size-unit'),
        size_convention=preferences.config.get('data-defaults', 'size-convention'),
        material=None,
        temperature=None,
        project_name=None,
        user_name=None,
        software_name=None,
        software_version=None,
        running_status=None,
        limit=None,
        stats_defined=None,
        images_present=None
    ):
    r"""
    Retreive models from the database.

    Args:
        session:          the database session

        geometry:         the name of the geometry to filter by, default value
                          is None indicating that geometry will not be used as
                          a filter.

        size:             the size of the geometry to filter by, the default
                          value is None indicating that geometry size will not
                          be used as a filter.

        size_unit:        the geometry size unit to filter by, the default
                          value is defined in config files, this filter is only
                          active if size is defined.

        size_convention:  the geometry size convention to filter by, the
                          default value is defined in config files, this filter
                          is only active if size id defined.

        material:         the name of the materisl to filter by, the default
                          is None indicating that material name will not be
                          used as a filter.

        temperature:      the temperature of the material to filter by,
                          the default is None indicating that the temperature
                          will not be used as a filter.

        project_name:     the name of the project to filter by, the default
                          value is None indicating that the project name
                          will not be used as a filter.

        user_name:        the name of the user to filter by, the default
                          value is None indicating that the user name will not
                          be used as a filter.

        software_name:    the name of the software to filter by, the default
                          value is None indicating that the user name will not
                          be used as a filter.

        software_version: the version of the software to filter by, the
                          default value is None indicating that the software
                          version will not be used as a filter.

        running_status:   a value indicating the running status.

        limit:            limits the number of records returned by the database,
                          by default this is None indicating that all records
                          will be returned.

        stats_defined:    a boolean value to indicate whether to filter models
                          by stats values that are defined  (true) or
                          undefined (false), the default value is None
                          indicating that stat values will not be used as a
                          filter.

        images_present:   a boolean value to indicate whether to filter models
                          by images that are present (true) or not present
                          (false), the default value is None indicating that
                          the presence of images will not be used as a filter.


    Returs:
        A list of database orm objects encapsulating the models that match
        the filters.

    Raises:
        None
    """

    # base sql
    sql = """
        select model.* from model
          inner join geometry
            on model.geometry_id = geometry.id
          inner join model_material_association
            on model_material_association.model_id = model.id
          inner join material
            on model_material_association.material_id = material.id
          inner join unit
            on geometry.size_unit_id = unit.id
          inner join size_convention
            on geometry.size_convention_id = size_convention.id
          inner join metadata
            on model.mdata_id = metadata.id
          inner join project
            on metadata.project_id = project.id
          inner join db_user
            on metadata.db_user_id = db_user.id
          inner join running_status
            on model.running_status_id = running_status.id
          inner join model_report_data
            on model.model_report_data_id = model_report_data.id
    """

    if software_name is not None or software_version is not None:
        sql = sql + """
          inner join software
            on metadata.software_id = software.id
        """

    # variable name to database name lookup
    vn_to_dn = {
        'geometry': 'geometry.name',
        'size': 'geometry.size',
        'material': 'material.name',
        'temperature': 'material.temperature',
        'size_unit': 'unit.symbol',
        'size_convention': 'size_convention.symbol',
        'project_name': 'project.name',
        'software_name': 'software.name',
        'software_version': 'software.version',
        'user_name': 'db_user.user_name',
        'running_status': 'running_status.name'
    }

    # add parameters if supplied by user
    params = {}
    if geometry:
        params['geometry'] = geometry
    if size:
        params['size'] = size
        params['size_unit'] = size_unit
        params['size_convention'] = size_convention
    if material:
        params['material'] = material
    if temperature:
        params['temperature'] = temperature
    if project_name:
        params['project_name'] = project_name
    if user_name:
        params['user_name'] = user_name
    if software_name:
        params['software_name'] = software_name
    if software_version:
        params['software_version'] = software_version
    if running_status:
        params['running_status'] = running_status

    #print(params)

    # build 'where clause'
    if params:
        sub_clauses = [
            '{dn:} = :{vn:}'.format(
                dn=vn_to_dn[key],
                vn=key
            ) for key in params.keys()
        ]

        sql = sql + ' where ' + ' and '.join(sub_clauses)

    # do we want models with stats defined or not
    if stats_defined is not None:
        if stats_defined == True:
            sql = sql + ''' and
                model.helicity_average is not null and
                model.helicity_integral is not null and
                model.helicity_minimum is not null and
                model.helicity_maximum is not null and
                model.m_rx is not null and
                model.m_ry is not null and
                model.m_rz is not null and
                model.total_energy is not null and
                model.demag_energy is not null and
                model.anis_energy is not null and
                model.exchange_energy is not null and
                model.external_energy is not null
            '''
        elif stats_defined == False:
            sql = sql + ''' and
                model.helicity_average is null and
                model.helicity_integral is null and
                model.helicity_minimum is null and
                model.helicity_maximum is null and
                model.m_rx is null and
                model.m_ry is null and
                model.m_rz is null and
                model.total_energy is null and
                model.demag_energy is null and
                model.anis_energy is null and
                model.exchange_energy is null and
                model.external_energy is null
            '''

    # do we want models with images present or not
    if images_present is not None:
        if images_present == True:
            sql = sql + ''' and
                model_report_data.has_xy_thumb_png is true and
                model_report_data.has_yz_thumb_png is true and
                model_report_data.has_xz_thumb_png is true and
                model_report_data.has_xy_png is true and
                model_report_data.has_yz_png is true and
                model_report_data.has_xz_png is true
            '''
        elif images_present == False:
            sql = sql + ''' and
                model_report_data.has_xy_thumb_png is false and
                model_report_data.has_yz_thumb_png is false and
                model_report_data.has_xz_thumb_png is false and
                model_report_data.has_xy_png is false and
                model_report_data.has_yz_png is false and
                model_report_data.has_xz_png is false
            '''

    # do we want to limit search results
    if limit:
        sql = sql + ' LIMIT {}'.format(limit)

    #print(params)
    #print(sql)

    dbo_models = session.query(orm.Model).from_statement(text(sql)).params(
        **params
    ).all()

    return dbo_models

