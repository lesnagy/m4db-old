r"""
Utility functions to retrieve running status information from the database.

"""
import orm

from sqlalchemy import text


def get(session,
        name
    ):
    r"""
    Retrieve statuses from the database.

    Args:
        session:  the database session.

        name:     the status name of the object we wish to retrieve.

    Returns:
        The status that matches the input filter
    """

    dbo_status = session.query(orm.RunningStatus).\
            filter(orm.RunningStatus.name == name).\
            first()

    return dbo_status

