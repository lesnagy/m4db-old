r"""
Utility functions to retrieve unit information from the database.

"""
import orm

from sqlalchemy import text

def get(session,
        symbol=None
    ):
    r"""
    Retrieve units from the database.

    Args:
        session:    the database session.

        symbol:     the unit symbol

    Returns:
        a list of unit orm objects encapsulating the units that match the input
        filters.

    Raises:
        none

    """
    query = session.query(orm.Unit)

    if symbol:
        query = query.filter(orm.Unit.symbol == symbol)

    dbo_units = query.all()

    return dbo_units
