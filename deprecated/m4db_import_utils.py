r"""
DEPRECATED!!! TODO [Les] remove/replace this

"""

import os, sys
import re
import uuid

from preferences import config

import subprocess

class MerrillModelScript:
    r'''
    Class to encapsulate a MERRILL model script.
    '''

    def __init__(self, **entries):

        self.max_energy_evaluations = None
        self.with_conjugate_gradient = False
        self.exchange_calculator = None,
        self.initial_field = None
        self.material = None
        self.temperature = None
        self.external_field_strength = None
        self.external_field_direction = None
        self.external_field_unit = None

        if 'file' in entries.keys():
            self.populate_from_file(entries['file'])
        else:
            entries2 = {k:v for k,v in entries.items()}
            del entries2['file']
            self.__dict__.update(entries2)


    def populate_from_file(self, file_name, strict=True):
        r'''
        Populate the object from a file.

        :param file_name: the name of the file.
        :param strict:    whether use strict rules (i.e. all entries must be
                          validated) when importing.

        '''
        str_float = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
        regex_max_energy_evaluations = re.compile(r'Set MaxEnergyEvaluations ([0-9]+)')
        regex_conjugate_gradient = re.compile(r'ConjugateGradient')
        regex_exchange_calculator = re.compile(r'Set ExchangeCalculator ([0-9]+)')
        regex_random_initial_field = re.compile(r'Randomize All Moments')
        regex_material_temperature = re.compile(r'((iron)|(magnetite))\s+([0-9]+)\s+C')
        regex_external_field_strength = re.compile(r'External Field Strength\s+({})\s+(.+)'.format(str_float))
        regex_external_field_direction = re.compile(r'External Field Direction ({}) ({}) ({})'.format(str_float, str_float, str_float))

        with open(file_name, 'r') as fin:
            for line in fin:
                line = line.strip()

                match_max_energy_evaluations = regex_max_energy_evaluations.match(line)
                if match_max_energy_evaluations:
                    self.max_energy_evaluations = int(match_max_energy_evaluations.group(1))
                    continue

                match_conjugate_gradient = regex_conjugate_gradient.match(line)
                if match_conjugate_gradient:
                    self.with_conjugate_gradient = True
                    continue

                match_exchange_calculator = regex_exchange_calculator.match(line)
                if match_exchange_calculator:
                    self.exchange_calculator = int(match_exchange_calculator.group(1))
                    continue

                match_random_initial_field = regex_random_initial_field.match(line)
                if match_random_initial_field:
                    self.initial_field = 'Random'
                    continue

                match_material_temperature = regex_material_temperature.match(line)
                if match_material_temperature:
                    self.material = match_material_temperature.group(1)
                    self.temperature = int(match_material_temperature.group(4))
                    continue

                match_external_field_strength = regex_external_field_strength.match(line)
                if match_external_field_strength:
                    self.external_field_strength = float(match_external_field_strength.group(1))
                    self.external_field_unit = match_external_field_strength.group(3)
                    continue

                match_external_field_direction = regex_external_field_direction.match(line)
                if match_external_field_direction:
                    self.external_field_direction = (
                        float(match_external_field_direction.group(1)),
                        float(match_external_field_direction.group(3)),
                        float(match_external_field_direction.group(5)),
                    )

        if strict:
            if self.max_energy_evaluations is None:
                raise ValueError('Invalid parse with strict mode: max_energy_evauations is None')
            elif self.with_conjugate_gradient is None:
                raise ValueError('Invalid parse with strict mode: with_conjugate_gradient is None')
            elif self.exchange_calculator is None:
                raise ValueError('Invalid parse with strict mode: exchange_calculator is None')
            elif self.initial_field is None:
                raise ValueError('Invalid parse with strict mode: initial_field is None')
            elif self.material is None:
                raise ValueError('Invalid parse with strict mode: material is None')
            elif self.temperature is None:
                raise ValueError('Invalid parse with strict mode: temperature is None')
            elif self.external_field_strength is None:
                raise ValueError('Invalid parse with strict mode: external_field_strength is None')
            elif self.external_field_direction is None:
                raise ValueError('Invalid parse with strict mode: external_field_direction is None')
            elif self.external_field_unit is None:
                raise ValueError('Invalid parse with strict mode: external_field_unit is None')


class MicromagneticMetadata:
    r'''
    Calculates the different energies and remanence vector for the magnetization.

    :param mesh_file:     the file containing mesh data (patran)
    :param field_file:    the file containing field data (dat)
    :param ext_field_strength: the external field strength used when calculating the
                               magnetization.
    :param ext_field_unit:     the unit of the external field.
    :paramext_field_direction: the direction that the external field had when
                               calculating the magnetization.

    Material parameters are supplied by the following named arguments

    :param material:    the name of the material used when calculating the
                        magnetization,
    :param temperature: the temperature of material

    alternatively the material parameters can be supplied directly

    :param K1: the magnetocrystalline anisotropy constant
    :param A:  the exchange constant
    :param Ms: the saturation magnetization constant
    '''

    def __init__(self, mesh_file, field_file, ext_field_strength, ext_field_unit, ext_field_direction, **kwargs):

        self.mesh_file = mesh_file
        self.field_file = field_file
        self.ext_field_strength = ext_field_strength
        self.ext_field_unit = ext_field_unit
        self.ext_field_direction = ext_field_direction

        script_file = str(uuid.uuid4())

        #######################################################################
        # Material branch of kwargs
        #######################################################################
        material = None
        temperature = None

        if 'material' in kwargs:
            if kwargs['material'] in ['iron', 'magnetite']:
                material = kwargs['material']
            else:
                raise ValueError(
                    'Material {} is currently unsupported!'.format(
                        kwargs['material']
                    )
                )

        if 'temperature' in kwargs:
            temperature = kwargs['temperature']

        #######################################################################
        # TODO: material constants branch of kwargs
        #######################################################################

        K1 = None
        A = None
        Ms = None

        # ... material constants branch code goes here ...

        #######################################################################
        # Select which branch to take
        #######################################################################

        if material is not None and temperature is not None:
            self.gen_script_from_material_and_temperature(
                script_file, material, temperature
            )
        elif K1 is not None and A is not None and Ms is not None:
            self.gen_script_from_material_parameters(
                script_file, K1, A, Ms
            )

        #######################################################################
        # Run the script                                                      #
        #######################################################################
        stdout = self.run_script(script_file)

        #######################################################################
        # Parse the output                                                    #
        #######################################################################
        self.anis_energy = None
        self.ext_energy = None
        self.demag_energy = None
        self.exch_energy = None
        self.mx = None
        self.my = None
        self.mz = None
        self.m = None

        self.parse_output(stdout)

        if self.anis_energy is None:
            raise ValueError("Could not calculate anisotropy energy")
        if self.ext_energy is None:
            raise ValueError("Could not calculate external energy")
        if self.demag_energy is None:
            raise ValueError("Could not calculate demag energy")
        if self.exch_energy is None:
            raise ValueError("Could not calculate exch energy")
        if self.mx is None:
            raise ValueError("Could not calculate mx")
        if self.my is None:
            raise ValueError("Could not calculate my")
        if self.mz is None:
            raise ValueError("Could not calculate mz")
        if self.m is None:
            raise ValueError("Could not calculate m")

        #######################################################################
        # Delete the script                                                   #
        #######################################################################

        os.remove(script_file)


    def gen_script_from_material_and_temperature(self, script_file, material, temperature):

        with open(script_file, 'w') as fout:
            fout.write("Set MaxMeshNumber 1\n")

            fout.write("ReadMesh 1 {}\n".format(
                os.path.join(
                    self.mesh_file
                )
            ))

            # If we're using a known material then pass those over to micromag
            # executable
            fout.write(
                '{} {} C\n'.format(
                    material,
                    temperature
                )
            )

            fout.write(
                "external field strength {} {}\n".format(
                    self.ext_field_strength,
                    self.ext_field_unit
                )
            )
            fout.write(
                "external field direction {} {} {}\n".format(
                    self.ext_field_direction[0],
                    self.ext_field_direction[1],
                    self.ext_field_direction[2]
                )
            )

            fout.write(
                "readmagnetization {}\n".format(
                    self.field_file
                )
            )

            fout.write("reportenergy\n")

            fout.write("end\n")

            fout.close()


    def gen_script_from_material_parameters(self, script_file, K1, A, Ms):
        # TODO: implement this
        pass


    def run_script(self, script_file):
        cmd = '{} {}'.format(
            config['external']['mm-solver'],
            script_file
        )

        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        )

        stdout, stderr = proc.communicate()

        stdout = stdout.decode('ascii')
        stderr = stderr.decode('ascii')

        return(stdout)

    def parse_output(self, stdout):
        flt = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
        do_energy_parse = False
        regex_energy_line = re.compile(r'Energies in units of J')
        regex_anis_energy = re.compile(r'E-Anis\s+({})'.format(flt))
        regex_ext_energy = re.compile(r'E-Ext\s+({})'.format(flt))
        regex_demag_energy = re.compile(r'E-Demag\s+({})'.format(flt))
        regex_exch_energy = re.compile(r'E-Exch\s+({})'.format(flt))

        do_magnetization_parse = False
        regex_magnetization_line = re.compile(r'Average magnetization')
        regex_mx = re.compile(r'<Mx>\s+({})'.format(flt))
        regex_my = re.compile(r'<My>\s+({})'.format(flt))
        regex_mz = re.compile(r'<Mz>\s+({})'.format(flt))
        regex_m  = re.compile(r'<M>\s+({})'.format(flt))

        lines = stdout.split('\n')
        for line in lines:
            ##########
            # Energy
            ##########
            match = regex_energy_line.search(line)
            if match:
                do_energy_parse = True
                continue

            match = regex_anis_energy.search(line)
            if do_energy_parse and match:
                self.anis_energy = float(match.group(1))
                continue

            match = regex_ext_energy.search(line)
            if do_energy_parse and match:
                self.ext_energy = float(match.group(1))
                continue

            match = regex_demag_energy.search(line)
            if do_energy_parse and match:
                self.demag_energy = float(match.group(1))
                continue

            match = regex_exch_energy.search(line)
            if do_energy_parse and match:
                self.exch_energy = float(match.group(1))
                continue

            ##########
            # magnetization
            ##########

            match = regex_magnetization_line.search(line)
            if match:
                do_magnetization_parse = True
                continue

            match = regex_mx.search(line)
            if do_magnetization_parse and match:
                self.mx = float(match.group(1))
                continue

            match = regex_my.search(line)
            if do_magnetization_parse and match:
                self.my = float(match.group(1))
                continue

            match = regex_mz.search(line)
            if do_magnetization_parse and match:
                self.mz = float(match.group(1))
                continue

            match = regex_m.search(line)
            if do_magnetization_parse and match:
                self.m = float(match.group(1))
                continue


if __name__ == '__main__':
    mm_data = MicromagneticMetadata(
        '/home/lnagy2/LegacyData/iron/cubes/meshes/10.pat',
        '/home/lnagy2/LegacyData/iron/cubes/lems/10nm/120C/10nm_120C_mag_0003.dat',
        0,
        'mT',
        (1,0,0),
        material='iron',
        temperature='120'
    )

    print(mm_data.anis_energy)
    print(mm_data.ext_energy)
    print(mm_data.demag_energy)
    print(mm_data.exch_energy)
    print(mm_data.mx)
    print(mm_data.my)
    print(mm_data.mz)
    print(mm_data.m)


