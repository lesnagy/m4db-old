r'''
Creates a logger object based on configuration file information, if no
configuration file exists, suitable defaults are used.

'''
import os

import preferences

import logging

def _select_logging_level(str_level):
    if str_level == 'debug':
        return logging.DEBUG
    elif str_level == 'info':
        return logging.INFO
    elif str_level == 'warning':
        return logging.WARNING
    elif str_level == 'error':
        return logging.ERROR
    elif str_level == 'critical':
        return logging.CRITICAL
    else:
        return logging.NOTSET

logfile_apath = os.path.join(
    preferences.config_file_apath,
    preferences.config['logging']['output']
)

logging.basicConfig(
    level=_select_logging_level(preferences.config['logging']['level']),
    filename=logfile_apath,
    filemode='a',
    format=preferences.config['logging']['format']
)


logger = logging.getLogger()

