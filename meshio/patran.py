r"""
Python implementation of a patran file reader.

"""

import re

import numpy as np


def read_patran_file(patran_file, indexing='one'):
    r"""
    Retrieves vertex and element data from a patran file.

    Args:
        patran_file: the absolute path name of the patran file to be read.

        indexing:    the type of indexing used in the patran file:
                        'one'  - if indexing begins from one
                        'zero' - if indexing begins from zero

    Returns: 
        a dictionary data structure containing patran file information with
        the following keys:
            'title'   - the title of the patran file
            'date'    - the date that the patran file was created
            'time'    - the time that the patran file was created
            'version' - the version of the patan file
            'nnodes'  - the number of nodes/vertices in the patran file
            'nodes'   - node information of the patran file, this is a 
                        nnodes x 3 numpy array of floating point values
                        where each triple represents a 3-dimensional 
                        node/vertex
            'nelems'  - the number of elements in the patran file
            'elems'   - element information of the patran file, this is a
                        nelems x 4 numpy array of integers where each 
                        4-tuple represents an element and each integer
                        of the 4-tuple is an index in to the 'nodes' array.
    
    Raises:
        ValueError: if the value of the 'indexing' parameter (see above) is not
                    recognised.
    """
    states = {
        'title': '25',
        'summary': '26',
        'node_data': '01',
        'element_data': '02',
        'material_properties': '03',
        'element_properties': '04',
        'coordinate_systems': '05',
        'distributed_element_loads': '06',
        'nodal_loads': '07',
        'terminate': '99'
    }

    state = None

    title = None
    date = None
    time = None
    version = None

    nnodes = None
    nelems = None
    nodes = None
    elems = None
    
    title_id = None
    title_nvalues = None
    title_line = None

    summary_id = None
    summary_nvalues = None
    summary_line = None

    node_id = None
    node_nvalues = None
    node_line = None

    elem_id = None
    elem_shape_id = None
    elem_nvalues = None
    elem_line = None

    material_id = None
    material_type_id = None
    material_nvalues = None
    material_line = None

    element_properties_id = None
    element_properties_nvalues = None
    element_properties_line = None

    with open(patran_file, 'r') as fin:
        for line in fin:
            #print(line)
            line = line.strip()
            card = line.split()

            if len(card) == 9 and card[0] == states['title']:
                state = 'title'
                title_line = 0
                title_id = int(card[1])
                title_nvalues = int(card[3])
                continue
            elif len(card) == 9 and card[0] == states['summary']:
                state = 'summary'
                summary_line = 0
                summary_id = int(card[1])
                summary_nvalues = int(card[2])
                nnodes = int(card[4])
                nelems = int(card[5])

                nodes = np.zeros((nnodes, 3), dtype=np.float64)
                elems = np.zeros((nelems, 4), dtype=np.uint64)

                continue
            elif len(card) == 9 and card[0] == states['node_data']:
                state = 'node_data'
                node_line = 0
                node_id = int(card[1])
                node_nvalues = int(card[2])
                continue
            elif len(card) == 9 and card[0] == states['element_data']:
                state = 'element_data'
                elem_line = 0
                elem_id = int(card[1])
                elem_shape_id = int(card[2])
                elem_nvalues = int(card[3])
                #print(elem_id)
                continue
            elif len(card) == 9 and card[0] == states['material_properties']:
                state = 'material_properties'
                material_line = 0
                material_id = int(card[1])
                material_type_id = int(card[2])
                material_values = int(card[3])
                continue
            elif len(card) == 9 and card[0] == states['element_properties']:
                state = 'element_properties'
                element_properties_line = 0
                element_properties_id = int(card[1])
                element_properties_nvalues = int(card[2])
                continue
            elif len(card) == 9 and card[0] == states['terminate']:
                #print("Terminating")
                break
            else:
                # At this point we didn't match an index and so we use the 
                # current state to interpret card/line data
                if state == 'title':
                    if title_line == 0:
                        title = line
                    title_line += 1
                    continue
                elif state == 'summary':
                    if summary_line == 0:
                        date = card[0]
                        time = card[1]
                        version = card[2]
                    summary_line += 1
                    continue
                elif state == 'node_data':
                    if node_line == 0:
                        nodes[node_id-1][0] = float(card[0])
                        nodes[node_id-1][1] = float(card[1])
                        nodes[node_id-1][2] = float(card[2])
                    elif node_line == 1:
                        #print("something beginning with 0G")
                        pass
                    node_line += 1
                    continue
                elif state == 'element_data':
                    if elem_line == 0:
                        #print("something to do with material")
                        pass
                    if elem_line == 1:
                        if indexing == 'one':
                            elems[elem_id-1][0] = int(card[0])-1
                            elems[elem_id-1][1] = int(card[1])-1
                            elems[elem_id-1][2] = int(card[2])-1
                            elems[elem_id-1][3] = int(card[3])-1
                        elif indexing == 'zero':
                            elems[elem_id-1][0] = int(card[0])-1
                            elems[elem_id-1][1] = int(card[1])-1
                            elems[elem_id-1][2] = int(card[2])-1
                            elems[elem_id-1][3] = int(card[3])-1
                        else:
                            raise ValueError(
                                "Unknown value for indexing: '{}'".format(
                                    indexing
                                )
                            )

                    elem_line += 1
                    continue
    
    return {
        'title': title,
        'date': date,
        'time': time,
        'version': version,
        'nnodes': nnodes,
        'nodes': nodes,
        'nelems': nelems,
        'elems': elems
    }


if __name__ == '__main__':
    # Test procedure.
    output = parse_patran_file(
        '/Users/lesleisnagy/Data/MMDatabaseTemp/iron/sphereE050pc/meshes/10.pat'
    )

    print(output)

