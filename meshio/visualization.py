r"""
Produce VTK objects that allow a vector field to be visualized.

TODO: [Les] This method used in this file is now broken and must be updated.

"""
import vtk
import re
import numpy as np

from log import logger

class VectorFieldSet:

    def __init__(self, fields, vertices, elements):
        r"""
        Initializes a VectorFieldSet object. This object builds VTK actors that
        may then be used in subsequent visualization.

        Args:
            fields:   an [NZ][N][3] array of data holding the moments at each
                      mesh vertex

            vertices: an [N][3] array of floats holiding the positions of
                      mesh vertices

            elements: an [NE][4] array of integers which are indices in to the
                      vertices array.

            where NZ is the number of zones, N is the number of vertices and
            NE is the number of elements.


        Returns:
            VectorFieldSet object.

        Raises:
            none

        """
        self.fields = fields
        self.vertices = vertices
        self.elements = elements

        self.ug = vtk.vtkUnstructuredGrid()
        self.pathSolutions = []
        self.gHMin = None
        self.gHMax = None
        self.gHAvg = None
        self.lut = None
        self.ctf = None
        self.currentZone = 0
        self.nzones = 0
        self.init()

        self.isoH = self.gHAvg

        #######################################################################
        # Arrow source format.                                                #
        #######################################################################

        self.arrowSource = vtk.vtkArrowSource()
        self.arrowSource.SetShaftResolution(30)
        self.arrowSource.SetTipResolution(50)

        self.transform = vtk.vtkTransform()
        self.transform.Translate([-0.5, 0.0, 0.0])

        self.transPolyData = vtk.vtkTransformPolyDataFilter()
        self.transPolyData.SetTransform(self.transform)
        self.transPolyData.SetInputConnection(self.arrowSource.GetOutputPort())

        #######################################################################
        # Vectors                                                             #
        #######################################################################

        # Create the glyphs
        self.glyph = vtk.vtkGlyph3D()
        self.glyph.SetSourceConnection(self.transPolyData.GetOutputPort())
        self.glyph.SetInputData(self.ug)
        self.glyph.ScalingOn()
        self.glyph.SetScaleModeToScaleByVector()
        self.glyph.SetVectorModeToUseVector()
        self.glyph.SetColorModeToColorByScalar()
        self.glyph.OrientOn()
        self.glyph.SetScaleFactor(0.020)
        self.glyph.Update()

        self.fieldMapper = vtk.vtkPolyDataMapper()
        self.fieldMapper.SetInputConnection(self.glyph.GetOutputPort())
        self.fieldMapper.ScalarVisibilityOn()
        self.fieldMapper.SetLookupTable(self.lut)
        self.fieldMapper.SetScalarRange(self.gHMin, self.gHMax)
        self.fieldMapper.Update()

        self.fieldActor = vtk.vtkActor()
        self.fieldActor.SetMapper(self.fieldMapper)

        #######################################################################
        # Helicity                                                            #
        #######################################################################

        self.isosurface = vtk.vtkContourGrid()
        self.isosurface.SetInputData(self.ug)
        self.isosurface.SetValue(0, self.isoH)

        self.isosurfaceMapper = vtk.vtkPolyDataMapper()
        self.isosurfaceMapper.ScalarVisibilityOff()
        self.isosurfaceMapper.SetInputData(self.isosurface.GetOutput())
        self.isosurfaceMapper.Update()

        self.isosurfaceActor = vtk.vtkActor()
        self.isosurfaceActor.SetMapper(self.isosurfaceMapper)
        self.isosurfaceActor.GetProperty().SetColor(1.0, 1.0, 1.0)
        self.isosurfaceActor.Modified()

        #######################################################################
        # Geometry                                                            #
        #######################################################################

        self.geometrySurface = vtk.vtkDataSetSurfaceFilter()
        self.geometrySurface.SetInputData(self.ug)
        self.geometrySurface.Update()

        self.geometryMapper = vtk.vtkDataSetMapper()
        self.geometryMapper.SetInputData(self.geometrySurface.GetOutput())
        self.geometryMapper.ScalarVisibilityOff()
        self.geometryMapper.Update()

        self.geometryActor = vtk.vtkActor()
        self.geometryActor.SetMapper(self.geometryMapper)
        self.geometryActor.Modified()

        #######################################################################
        # Wireframe                                                           #
        #######################################################################

        self.wireframeActor = vtk.vtkActor()
        self.wireframeActor.SetMapper(self.geometryMapper)
        self.wireframeActor.GetProperty().SetRepresentationToWireframe()
        self.wireframeActor.GetProperty().SetAmbient(1)
        self.wireframeActor.GetProperty().SetDiffuse(0)
        self.wireframeActor.Modified()

        # Display the first point in the path by default.
        self.setCurrentZone(0)

    def init(self):

        # Vertices.
        points = vtk.vtkPoints()
        points.SetNumberOfPoints(self.vertices.shape[0])
        for i in range(self.vertices.shape[0]):
            points.SetPoint(i,
                self.vertices[i,0],
                self.vertices[i,1],
                self.vertices[i,2]
            )
        self.ug.SetPoints(points)

        # Connectivity
        for i in range(self.elements.shape[0]):
            element = vtk.vtkIdList()

            element.InsertNextId(self.elements[i][0])
            element.InsertNextId(self.elements[i][1])
            element.InsertNextId(self.elements[i][2])
            element.InsertNextId(self.elements[i][3])

            self.ug.InsertNextCell(vtk.VTK_TETRA, element)

        # Magnetizations in all the zones.
        for zone in range(self.fields.shape[0]):
            field = vtk.vtkDoubleArray()
            field.SetName(self.pathIdNameM(zone))
            field.SetNumberOfComponents(3)
            field.SetNumberOfTuples(self.field.shape[1])

            self.pathSolutions.append(self.pathIdNameM(zone))

            for i in range(self.fields.shape[1]):
                field.SetTuple3(i,
                    self.field[zone][i][0],
                    self.field[zone][i][1],
                    self.field[zone][i][2])

            self.ug.GetPointData().AddArray(field)

        # Helicities in all zones.
        self.gHMin =  1E100
        self.gHMax = -1E100
        self.gHAvg = 0.0
        for zone in range(self.field.shape[0]):
            vorticity = vtk.vtkGradientFilter()
            vorticity.ComputeVorticityOn()
            vorticity.SetInputArrayToProcess(0, 0, 0, 0, self.pathIdNameM(zone))
            vorticity.SetResultArrayName('V')
            vorticity.SetInputData(self.ug)
            vorticity.Update()

            helicity = vtk.vtkArrayCalculator()
            helicity.SetAttributeModeToUsePointData()
            helicity.AddVectorArrayName(self.pathIdNameM(zone))
            helicity.AddVectorArrayName('V')
            helicity.SetResultArrayName('H')
            helicity.SetFunction('{}.V'.format(self.pathIdNameM(zone)))
            helicity.SetInputData(vorticity.GetOutput())
            helicity.Update()

            helicityData = helicity.GetOutput().GetPointData().GetArray('H')

            helicityField = vtk.vtkDoubleArray()
            helicityField.SetName(self.pathIdNameH(zone))
            helicityField.SetNumberOfComponents(1)
            helicityField.SetNumberOfTuples(helicityData.GetNumberOfTuples())

            for i in range(helicityField.GetNumberOfTuples()):

                # Minimum/maximum
                if helicityData.GetValue(i) < self.gHMin:
                    self.gHMin = helicityData.GetValue(i)
                if helicityData.GetValue(i) > self.gHMax:
                    self.gHMax = helicityData.GetValue(i)

                # Add helicity to field
                helicityField.SetValue(i, helicityData.GetValue(i))

                # Accumulate helicity values in average
                self.gHAvg += helicityData.GetValue(i)

            self.ug.GetPointData().AddArray(helicityField)

        # divide by (no. of zones)*(no. of field points)
        logger.debug('Global helicity total:   {}'.format(self.gHAvg))
        logger.debug('Global helicity minimum: {}'.format(self.gHMin))
        logger.debug('Global helicity maximum: {}'.format(self.gHMax))

        self.gHAvg /= (float(self.fields.shape[0])*float(self.fields.shape[1]))

        logger.debug('Global helicity average: {}'.format(self.gHAvg))

        # LUTs
        self.lut = vtk.vtkLookupTable()
        self.ctf = vtk.vtkColorTransferFunction()
        nlut = 1000
        dHelicity = abs(self.gHMax-self.gHMin)/float(nlut)

        self.lut.SetNumberOfTableValues(nlut)

        self.ctf.AddRGBSegment(
                (self.gHMin)               , 0.0, 0.0, 1.0,
                (self.gHMin+self.gHMax)/2.0, 1.0, 1.0, 1.0)
        self.ctf.AddRGBSegment(
                (self.gHMin+self.gHMax)/2.0, 1.0, 1.0, 1.0,
                (self.gHMax)               , 1.0, 0.0, 0.0)

        for i in range(nlut):
            scalar = self.gHMin + float(i)*dHelicity
            cc = self.ctf.GetColor(scalar)
            self.lut.SetTableValue(i, cc[0], cc[1], cc[2])

    def pathIdNameM(self, pathId):
        return 'm_{}'.format(pathId)

    def pathIdNameH(self, pathId):
        return 'h_{}'.format(pathId)

    def pathId(self, pathId):
        return int(pathId.split('_')[1])

    def getFieldActor(self):
        return self.fieldActor

    def getIsosurfaceActor(self):
        return self.isosurfaceActor

    def getGeometryActor(self):
        return self.geometryActor

    def getWireframeActor(self):
        return self.wireframeActor

    def setCurrentZone(self, zone):
        self.currentZone = zone
        self.ug.GetPointData().SetActiveVectors(self.pathIdNameM(self.currentZone))
        self.ug.GetPointData().SetActiveScalars(self.pathIdNameH(self.currentZone))
        self.geometryMapper.Update()
        self.isosurfaceMapper.Update()
        self.fieldMapper.Update()

    def getCurrentZone(self):
        return self.currentZone

    def getNZones(self):
        return self.nzones

    def nextModel(self):
        logger.debug('nextModel(), zone was {}'.format(self.currentZone))
        if self.currentZone+1 < self.nzones:
            self.setCurrentZone(self.currentZone+1)

        logger.debug('nextModel(), and now zone is {}'.format(self.currentZone))

    def previousModel(self):
        logger.debug('previousModel(), zone was {}'.format(self.currentZone))
        if -1 < self.currentZone-1:
            self.setCurrentZone(self.currentZone-1)

        logger.debug('previousModel(), and now zone is {}'.format(self.currentZone))

    def jumpToModel(self, index):
        logger.debug("MicromagneticModel.jumpToModel(): {}".format(index))
        self.setCurrentZone(index)

class MeshGeometry:

    def __init__(self, vertices, elements):

        self.vertices = vertices
        self.elements = elements

        self.ug = vtk.vtkUnstructuredGrid()
        self.init()

        #######################################################################
        # Geometry                                                            #
        #######################################################################

        self.geometrySurface = vtk.vtkDataSetSurfaceFilter()
        self.geometrySurface.SetInputData(self.ug)
        self.geometrySurface.Update()

        self.geometryMapper = vtk.vtkDataSetMapper()
        self.geometryMapper.SetInputData(self.geometrySurface.GetOutput())
        self.geometryMapper.ScalarVisibilityOff()
        self.geometryMapper.Update()

        self.geometryActor = vtk.vtkActor()
        self.geometryActor.SetMapper(self.geometryMapper)
        self.geometryActor.Modified()


    def init(self):

        # Vertices.
        points = vtk.vtkPoints()
        points.SetNumberOfPoints(self.vertices.shape[0])
        for i in range(self.vertices.shape[0]):
            points.SetPoint(i,
                self.vertices[i,0],
                self.vertices[i,1],
                self.vertices[i,2]
            )
        self.ug.SetPoints(points)

        # Connectivity
        for i in range(self.elements.shape[0]):
            element = vtk.vtkIdList()

            element.InsertNextId(self.elements[i][0])
            element.InsertNextId(self.elements[i][1])
            element.InsertNextId(self.elements[i][2])
            element.InsertNextId(self.elements[i][3])

            self.ug.InsertNextCell(vtk.VTK_TETRA, element)


    def getGeometryActor(self):
        return self.geometryActor
