"""
Created on Aug 25, 2017

@author: Lesleis Nagy

"""

from datetime import datetime
from math import sin, cos, acos, atan2, pi
from uuid import uuid4

from sqlalchemy import Table, Column, Integer, Float, String, DateTime, Boolean
from sqlalchemy import ForeignKey
from sqlalchemy import UniqueConstraint
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import column_property
from sqlalchemy.orm import relationship

import json

now = datetime.now


Base = declarative_base()


def new_unique_id():
    return str(uuid4())


class DBUser(Base):
    """
    Holds information about who a model belongs to. NOTE: this is not ownership
    in the sense of permissions but it gives us a useful dimension to 
    differentiate different models.

    Attributes:
        id            : a unique internal id for the object

        user_name     : a unique name for the user

        first_name    : the user's first name

        initials      : the user's middle initials

        surname       : the user's surname

        email         : the user's surname

        telephone     : the user's telephone

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record
    """
    __tablename__ = 'db_user'

    id = Column(Integer, primary_key=True)
    user_name = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
    initials = Column(String, nullable=True)
    surname = Column(String, nullable=False)
    email = Column(String, nullable=False)
    telephone = Column(String, nullable=True)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('first_name', 'surname', 'email', 'telephone', name='uniq_user_01')
    UniqueConstraint('user_name', name='uniq_user_02')


class Software(Base):
    """
    Holds information about the software used to run models.

    Attributes:
        id            : a unique internal id for the object

        name          : the name of the software package

        version       : the version of the software package

        description   : a description of the software package

        url           : a URL for the software package

        citation      : a citation for the software package

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record

    """
    __tablename__ = 'software'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    version = Column(String, nullable=False)
    description = Column(String, nullable=True)
    url = Column(String, nullable=True)
    citation = Column(String, nullable=True)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('name', 'version', name='uniq_software_01')


class Unit(Base):
    """
    Holds a number of units used to give the sizes/magnitudes of some values
    found in the database.

    Attributes:
        id            : a unique internal id for the object

        symbol        : the symbol associated with the unit

        name          : the unit's name

        power         : the power/exponent of the unit (in the natural base 
                        of the unit)

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record
        
    """
    __tablename__ = 'unit'

    id = Column(Integer, primary_key=True)
    symbol = Column(String, nullable=False)
    name = Column(String, nullable=False)
    power = Column(Float, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('symbol', name='uniq_unit_01')


class PhysicalConstant(Base):
    """
    Holds a number of important physical constants.

    Attributes:
        id            : a unique internal id for the object

        symbol        : the symbol associated with the physical constant

        name          : the constant's name

        value         : the constant's value

        unit          : the symbolic unit for the contant

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record

    """
    __tablename__ = 'physical_constant'

    id = Column(Integer, primary_key=True)
    symbol = Column(String, nullable=False)
    name = Column(String, nullable=False)
    value = Column(Float, nullable=False)
    unit = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('symbol', name='uniq_physica_constant_01')


class SizeConvention(Base):
    """
    Holds a size convention, size conventions are useful since we use them as
    a short hand to refer to volumentric sizes. For example: what do we mean by
    "100nm grain"?, if we're using equivalent spherical volume diameter (ESVD)
    then the grain has a volume equivalent to a sphere of diameter 100nm.

    Attributes:
        id            : a unique internal id for the object

        symbol        : the symbol associated with the size convention

        description   : a description for the size convention

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record

    """
    __tablename__ = 'size_convention'

    id = Column(Integer, primary_key=True)
    symbol = Column(String, nullable=False)
    description = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('symbol', name='uniq_size_convention_01')


class AnisotropyForm(Base):
    """
    Hold different anisotropy forms for example 'cubic' and 'uniaxial'.

    Attributes:
        id            : a unique internal id for the object

        name          : anisotropy form's name

        description   : a description for the anisotropy form convention

        last_modified : the date/time at which this object/record was modified

        created       : the creation date/time of this object/record

    """
    __tablename__ = 'anisotropy_form'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    UniqueConstraint('name', name='uniq_anisotropy_form_01')


class Geometry(Base):
    """
    Hold's geometry data. The natural key for a geometry is name/size pair
    (hence the reason why size is a string). Note: size is given as a string
    decimal of the form '[0-9]*\.[0-9]+', and is assumed to have the units
    defined by size_unit.

    Attributes:
        id                       : a unique internal id for the object
        
        unique_id                : a unique identifier for the geometry that 
                                   may be used outside the database.

        name                     : the name of the geometry

        size                     : the size of the geometry in the given units

        element_size             : the size of the elements that comprise this 
                                   geometry

        description              : a brief description of the geometry

        nelements                : the number of elements that comprise the 
                                   geometry

        nvertices                : the number of vertices that comprise the 
                                   geometry

        element_volume_average   : the average volume over elements

        element_volume_std       : the standard deviation volume over elements

        element_distance_average : the average distance between vertices

        element_distance_std     : the standard deviation of distance between 
                                   vertices
        
        has_patran               : a patran file is available for the geometry

        has_exodus               : an exodus file is available for the geometry

        has_mesh_gen_script      : a script to generate the mesh is available
                                   for the geometry

        last_modified            : the date/time at which this object/record 
                                   was modified

        created                  : the creation date/time of this object/record
    """
    __tablename__ = 'geometry'

    id = Column(Integer, primary_key=True, nullable=False)
    unique_id = Column(String, default=new_unique_id, nullable=False)
    name = Column(String, nullable=False)
    size = Column(Integer, nullable=False)
    element_size = Column(Integer, nullable=True)
    description = Column(String, nullable=True)
    nelements = Column(Integer, nullable=False)
    nvertices = Column(Integer, nullable=False)
    volume_total = Column(Float, nullable=True)
    element_volume_average = Column(Float, nullable=True)
    element_volume_std = Column(Float, nullable=True)
    element_distance_average = Column(Float, nullable=True)
    element_distance_std = Column(Float, nullable=True)
    has_patran = Column(Boolean, default=False, nullable=False)
    has_exodus = Column(Boolean, default=False, nullable=False)
    has_mesh_gen_script = Column(Boolean, default=False, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    size_unit_id = Column(Integer, ForeignKey('unit.id'), nullable=False)
    size_unit = relationship('Unit', uselist=False, foreign_keys=[size_unit_id])

    element_size_unit_id = Column(Integer, ForeignKey('unit.id'), nullable=True)
    element_size_unit = relationship('Unit', uselist=False, foreign_keys=[element_size_unit_id])

    size_convention_id = Column(Integer, ForeignKey('size_convention.id'), nullable=False)
    size_convention = relationship('SizeConvention', uselist=False)

    # The unit's that were assumed when the mesh was created 
    mesh_intrinsic_unit_id = Column(Integer, ForeignKey('unit.id'), nullable=False)
    mesh_intrinsic_unit = relationship('Unit', uselist=False, foreign_keys=[mesh_intrinsic_unit_id])

    UniqueConstraint(
        'name',
        'size',
        'element_size',
        'size_unit_id',
        'element_size_unit_id',
        'nelements',
        'nvertices',
        name='uniq_geometry_01'
    )

    UniqueConstraint('unique_id', name='uniq_geometry_02')

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Material(Base):
    """Holds material data. The natural key for a material is name/temperature
    pair (hence the reason why temperature is a string). Note: temperature is
    given as a string decimal of the form '[0-9]*\.[0-9]+', and is also *ALWAYS*
    assumed to be in degrees Celsius.

    """
    __tablename__ = 'material'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    temperature = Column(Integer, nullable=False)
    k1 = Column(Float, nullable=False)
    a = Column(Float, nullable=False)
    ms = Column(Float, nullable=False)
    kd = Column(Float, nullable=False)
    lambda_ex = Column(Float, nullable=True)
    q_hardness = Column(Float, nullable=True)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    anisotropy_form_id = Column(Integer, ForeignKey('anisotropy_form.id'), nullable=False)
    anisotropy_form = relationship('AnisotropyForm', uselist=False)

    UniqueConstraint('name', 'temperature', name='uniq_material_01')


class Field(Base):
    """The parent class/entity of all fields. This field object/entity should
    *NOT* be used on its own - please use one of the derived types:
        1) FileField
        2) ModelField
        3) RandomField
        4) UniformField

    """
    __tablename__ = 'field'

    id = Column(Integer, primary_key=True, nullable=False)
    unique_id = Column(String, default=new_unique_id, nullable=False)
    type = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'field',
        'polymorphic_on': type
    }

    UniqueConstraint('unique_id', name='uniq_field_01')


class FileField(Field):
    """A file field is a field that derives its structure from the data stored
    in a file.

    """
    __tablename__ = 'file_field'

    id = Column(Integer, ForeignKey('field.id'), primary_key=True, nullable=False)
    last_modified = column_property(Column(DateTime, default=now(), onupdate=now(), nullable=False),
                                    Field.last_modified)
    created = column_property(Column(DateTime, default=now(), nullable=False), Field.created)

    __mapper_args__ = {
        'polymorphic_identity': 'file_field'
    }


class ModelField(Field):
    """A model field is a field that that derives its structure from an existing
    model.

    """
    __tablename__ = 'model_field'

    id = Column(Integer, ForeignKey('field.id'), primary_key=True, nullable=False)
    last_modified = column_property(Column(DateTime, default=now(), onupdate=now(), nullable=False),
                                    Field.last_modified)
    created = column_property(Column(DateTime, default=now(), nullable=False), Field.created)

    model_id = Column(Integer, ForeignKey('model.id'), nullable=False)
    model = relationship('Model')

    __mapper_args__ = {
        'polymorphic_identity': 'model_field',
    }


class RandomField(Field):
    """A random field is really just a placeholder record to denote a random
    field.  Actual seeds my be stored if wanting to reproduce specific random
    fields (but this is not cross platform compatible).

    """
    __tablename__ = 'random_field'

    id = Column(Integer, ForeignKey('field.id'), primary_key=True, nullable=False)
    seed = Column(Integer, nullable=True)
    last_modified = column_property(Column(DateTime, default=now(), onupdate=now(), nullable=False),
                                    Field.last_modified)
    created = column_property(Column(DateTime, default=now(), nullable=False), Field.created)

    __mapper_args__ = {
        'polymorphic_identity': 'random_field',
    }


class UniformField(Field):
    """A UniformField consists of a vector in spherical polar coordinates using
    using (radial, azimuthal, polar) = (magnitude, theta, phi) convention.
    Internally theta and phi are stored in radians, but helper functions are
    provided that convert between Cartesian/spherical-polar and degrees and
    radians.

    """
    __tablename__ = 'uniform_field'

    id = Column(Integer, ForeignKey('field.id'), primary_key=True, nullable=False)
    theta = Column(Float, nullable=False)
    phi = Column(Float, nullable=False)
    magnitude = Column(Float, nullable=False)
    last_modified = column_property(Column(DateTime, default=now(), onupdate=now(), nullable=False),
                                    Field.last_modified)
    created = column_property(Column(DateTime, default=now(), nullable=False), Field.created)

    unit_id = Column(Integer, ForeignKey('unit.id'), nullable=False)
    unit = relationship('Unit')

    __mapper_args__ = {
        'polymorphic_identity': 'uniform_field',
    }

    def __init__(self, **kwargs):
        use_degrees = False
        if 'use_degrees' in kwargs.keys():
            use_degrees = kwargs['use_degrees']

        if 'theta' in kwargs.keys() and 'phi' in kwargs.keys() and 'magnitude' in kwargs.keys():
            # Initialise based on spherical-polar.
            if use_degrees:
                self.theta = self.degree_to_radian(kwargs['theta'])
                self.phi = self.degree_to_radian(kwargs['phi'])
                self.magnitude = kwargs['magnitude']
            else:
                self.theta = kwargs['theta']
                self.phi = kwargs['phi']
                self.magnitude = kwargs['magnitude']
        elif 'dx' in kwargs.keys() and 'dy' in kwargs.keys() and 'dz' in kwargs.keys() and 'magnitude' in kwargs.keys():
            # Initialise based on Cartesian.
            dx = kwargs['dx']
            dy = kwargs['dy']
            dz = kwargs['dz']

            (theta, phi) = UniformField.cartesian_to_spherical_direction(dx, dy, dz)

            self.theta = theta
            self.phi = phi
            self.magnitude = kwargs['magnitude']
        else:
            raise ValueError(
                "UniformField must be defined in therms of ('magnitude', 'theta', 'phi')" 
                " OR ('dx', 'dy', 'dz', 'magnitude')")

        if 'unit' in kwargs.keys():
            self.unit = kwargs['unit']

    @hybrid_property
    def cartesian_direction(self):
        r"""Access the direction of the UniformField vector using Cartesian representation.

        """
        return UniformField.spherical_to_cartesian_direction(self.theta, self.phi)

    @hybrid_property
    def degree_direction(self):
        r"""Retrieve the directional component (theta, phi) of the UniformField in units of degree.

        """
        return self.radian_to_degree(self.theta), self.radian_to_degree(self.phi)

    @cartesian_direction.setter
    def cartesian_direction(self, **kwargs):
        r"""Set the direction of a UniformField vector to correspond to the new direction defined in Cartesian
        components.

        """
        if 'dx' in kwargs.keys() and 'dy' in kwargs.keys() and 'dz' in kwargs.keys():
            # Only change the direction.
            dx = kwargs['dx']
            dy = kwargs['dy']
            dz = kwargs['dz']
            theta, phi = UniformField.cartesian_to_spherical_direction(dx, dy, dz)
            self.theta = theta
            self.phi = phi

    @degree_direction.setter
    def degree_direction(self, **kwargs):
        r"""Set the internal representation of the UniformField vector to
        correspond to the new field y-component value.

        """
        self.theta = UniformField.degree_to_radian(kwargs['theta'])
        self.phi = UniformField.degree_to_radian(kwargs['phi'])

    @staticmethod
    def spherical_to_cartesian_direction(theta, phi):
        """Private function to convert sphercal-polar coordinate to Cartesian.

        """

        x = cos(theta) * sin(phi)
        y = sin(theta) * sin(phi)
        z = cos(phi)

        return x, y, z

    @staticmethod
    def cartesian_to_spherical_direction(x, y, z):
        """Private function to convert Cartesian coordinate to spheical-polar.

        """
        theta = atan2(y, x)
        phi = acos(z)        # Don't divide by radial component here since we assume x,y,z specifies *ONLY* a direction.

        return theta, phi

    @staticmethod
    def degree_to_radian(value):
        return value * (pi / 180.0)

    @staticmethod
    def radian_to_degree(value):
        return value * (180.0 / pi)


class RunningStatus(Base):
    __tablename__ = 'running_status'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class RunningLocation(Base):
    __tablename__ = 'running_location'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    url = Column(String, nullable=False)
    base_directory = Column(String, nullable=False)
    ssh_key = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class ModelRunData(Base):
    __tablename__ = 'model_run_data'

    id = Column(Integer, primary_key=True, nullable=False)
    has_script = Column(Boolean, default=False, nullable=False)
    has_stdout = Column(Boolean, default=False, nullable=False)
    has_stderr = Column(Boolean, default=False, nullable=False)
    has_energy_log = Column(Boolean, default=False, nullable=False)
    has_tecplot = Column(Boolean, default=False, nullable=False)
    has_json = Column(Boolean, default=False, nullable=False)
    has_dat = Column(Boolean, default=False, nullable=False)
    has_helicity_dat = Column(Boolean, default=False, nullable=False)
    has_vorticity_dat = Column(Boolean, default=False, nullable=False)
    has_anisotropy_energy_density_dat = Column(Boolean, default=False, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class ModelReportData(Base):
    __tablename__ = 'model_report_data'

    id = Column(Integer, primary_key=True, nullable=False)
    has_xy_thumb_png = Column(Boolean, default=False, nullable=False)
    has_yz_thumb_png = Column(Boolean, default=False, nullable=False)
    has_xz_thumb_png = Column(Boolean, default=False, nullable=False)
    has_xy_png = Column(Boolean, default=False, nullable=False)
    has_yz_png = Column(Boolean, default=False, nullable=False)
    has_xz_png = Column(Boolean, default=False, nullable=False)


class Project(Base):
    __tablename__ = 'project'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)

    UniqueConstraint('project_name', name='uniq_project_01')


class Metadata(Base):
    __tablename__ = 'metadata'

    id = Column(Integer, primary_key=True, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    project = relationship('Project')

    db_user_id = Column(Integer, ForeignKey('db_user.id'), nullable=False)
    db_user = relationship('DBUser')

    software_id = Column(Integer, ForeignKey('software.id'), nullable=True)
    software = relationship('Software')


class LegacyModelInfo(Base):
    __tablename__ = 'legacy_model_info'

    id = Column(Integer, primary_key=True, nullable=False)
    index = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class ExchangeCalculation(Base):
    __tablename__ = 'exchange_calculation'

    id = Column(Integer, primary_key=True, nullable=False)
    description = Column(String, nullable=False)
    script_id = Column(Integer, nullable=False)

    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


model_material_association = Table(
    'model_material_association',
    Base.metadata,
    Column('model_id', Integer, ForeignKey('model.id')),
    Column('material_id', Integer, ForeignKey('material.id'))
)


class Model(Base):
    __tablename__ = 'model'

    id = Column(Integer, primary_key=True, nullable=False)
    unique_id = Column(String, default=new_unique_id, nullable=False)
    helicity_average = Column(Float, nullable=True)
    helicity_integral = Column(Float, nullable=True)
    helicity_minimum = Column(Float, nullable=True)
    helicity_maximum = Column(Float, nullable=True)
    m_rx = Column(Float, nullable=True)
    m_ry = Column(Float, nullable=True)
    m_rz = Column(Float, nullable=True)
    total_energy = Column(Float, nullable=True)
    demag_energy = Column(Float, nullable=True)
    anis_energy = Column(Float, nullable=True)
    exchange_energy = Column(Float, nullable=True)
    external_energy = Column(Float, nullable=True)
    max_energy_evaluations = Column(Integer, nullable=False)
    with_conjugate_gradient = Column(Boolean, default=False, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    geometry_id = Column(Integer, ForeignKey('geometry.id'), nullable=False)
    geometry = relationship('Geometry', uselist=False)

    materials = relationship("Material", secondary=model_material_association)

    start_magnetization_id = Column(Integer, ForeignKey('field.id'), nullable=True)
    start_magnetization = relationship('Field', uselist=False, foreign_keys=[start_magnetization_id])

    external_field_id = Column(Integer, ForeignKey('field.id'), nullable=False)
    external_field = relationship('Field', uselist=False, foreign_keys=[external_field_id])

    running_status_id = Column(Integer, ForeignKey('running_status.id'), nullable=False)
    running_status = relationship('RunningStatus', uselist=False)

    model_run_data_id = Column(Integer, ForeignKey('model_run_data.id'), nullable=False)
    model_run_data = relationship('ModelRunData', uselist=False)

    model_report_data_id = Column(Integer, ForeignKey('model_report_data.id'), nullable=False)
    model_report_data = relationship('ModelReportData', uselist=False)

    mdata_id = Column(Integer, ForeignKey('metadata.id'), nullable=False)
    mdata = relationship('Metadata', uselist=False)

    legacy_model_info_id = Column(Integer, ForeignKey('legacy_model_info.id'), nullable=True)
    legacy_model_info = relationship('LegacyModelInfo', uselist=False)

    exchange_calculation_id = Column(Integer, ForeignKey('exchange_calculation.id'), nullable=False)
    exchange_calculation = relationship('ExchangeCalculation', uselist=False)

    running_location_id = Column(Integer, ForeignKey('running_location.id'), nullable=True)
    running_location = relationship('RunningLocation', uselist=False)

    paths = relationship('PathModelSplit', back_populates='model')


class PathCalculation(Base):
    r"""
    The type of path calculation used to calculate the data associated with a
    Path. For example:
       neb          Path calculation based on the Nudged Elastic Band (NEB)
       fs_heuristic Path calculation based on Fabian & Shcherbakov (arXiv:1702.00070v1)

    """
    __tablename__ = 'path_calculation'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class PathRunData(Base):
    __tablename__ = 'path_run_data'

    id = Column(Integer, primary_key=True, nullable=False)
    has_script = Column(Boolean, default=False, nullable=False)
    has_stdout = Column(Boolean, default=False, nullable=False)
    has_stderr = Column(Boolean, default=False, nullable=False)
    has_energy_log = Column(Boolean, default=False, nullable=False)
    has_tecplot = Column(Boolean, default=False, nullable=False)
    has_path_energies = Column(Boolean, default=False, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class PathReportData(Base):
    __tablename__ = 'path_report_data'

    id = Column(Integer, primary_key=True, nullable=False)
    has_x_thumb_png = Column(Boolean, default=False, nullable=False)
    has_y_thumb_png = Column(Boolean, default=False, nullable=False)
    has_z_thumb_png = Column(Boolean, default=False, nullable=False)
    has_x_png = Column(Boolean, default=False, nullable=False)
    has_y_png = Column(Boolean, default=False, nullable=False)
    has_z_png = Column(Boolean, default=False, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class PathStartEndHeuristic(Base):
    __tablename__ = 'path_start_end_heuristic'

    id = Column(Integer, primary_key=True, nullable=False)
    start_end_remanence_angle = Column(Float, nullable=True)
    start_end_remanence_angle_error = Column(Float, nullable=True)
    start_end_helicity = Column(Float, nullable=True)
    start_end_helicity_error = Column(Float, nullable=True)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)


class Path(Base):
    __tablename__ = 'path'

    id = Column(Integer, primary_key=True, nullable=False)
    unique_id = Column(String, default=new_unique_id, nullable=False)
    spring_constant = Column(Float, nullable=True)
    curvature_weight = Column(Float, nullable=True)
    no_of_points = Column(Integer, nullable=True)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    start_model_id = Column(Integer, ForeignKey('model.id'), nullable=False)
    start_model = relationship('Model', foreign_keys=[start_model_id])

    end_model_id = Column(Integer, ForeignKey('model.id'), nullable=False)
    end_model = relationship('Model', foreign_keys=[end_model_id])

    parent_path_id = Column(Integer, ForeignKey('path.id'), default=None, nullable=True)
    parent_path = relationship('Path', foreign_keys=[parent_path_id], remote_side=[id])

    path_calculation_id = Column(Integer, ForeignKey('path_calculation.id'), nullable=False)
    path_calculation = relationship('PathCalculation')

    path_run_data_id = Column(Integer, ForeignKey('path_run_data.id'), nullable=False)
    path_run_data = relationship('PathRunData')

    path_report_data_id = Column(Integer, ForeignKey('path_report_data.id'), nullable=False)
    path_report_data = relationship('PathReportData', uselist=False)

    running_status_id = Column(Integer, ForeignKey('running_status.id'), nullable=False)
    running_status = relationship('RunningStatus', uselist=False)

    mdata_id = Column(Integer, ForeignKey('metadata.id'), nullable=False)
    mdata = relationship('Metadata', uselist=False)

    path_start_end_heuristic_id = Column(
        Integer,
        ForeignKey('path_start_end_heuristic.id'),
        default=None,
        nullable=True
    )
    path_start_end_heuristic = relationship('PathStartEndHeuristic', uselist=False)

    models = relationship('PathModelSplit', back_populates='path')

    UniqueConstraint('unique_id', name='uniq_path_01')


# noinspection PyUnusedLocal
def validate_path(mapper, conection, value):
    if not value.parent_path:
        if value.start_model.geometry.id != value.end_model.geometry.id:
            raise ValueError('Path does not have start/end models with same geometry')

        # Start and end must have same no. of materials
        if len(value.start_model.materials) != len(value.end_model.materials):
            raise ValueError('Path does not have start/end models with same material. '
                             'No of start model materials: {}, no. of end model materials: {}'.format(
                ' '.join(map(lambda x : '{} {}'.format(x.name, x.temperature), value.start_model.materials)),
                ' '.join(map(lambda x : '{} {}'.format(x.name, x.temperature), value.end_model.materials))
            ))

        # Start and end must have same materials
        for start_material, end_material in zip(value.start_model.materials, value.end_model.materials):
            if start_material.id != end_material.id:
                raise ValueError('Path does not have start/end models with same material. '
                                 'Start model id: {}, end model id: {}'.format(start_material.id, end_material.id))

        # TODO: add test here to check external_field similar (???)
event.listen(Path, 'before_insert', validate_path)


class PathModelSplit(Base):
    __tablename__ = 'path_model_split'

    index = Column(Integer, nullable=False)
    last_modified = Column(DateTime, default=now(), onupdate=now(), nullable=False)
    created = Column(DateTime, default=now(), nullable=False)

    path_id = Column(Integer, ForeignKey('path.id'), primary_key=True, nullable=False)
    path = relationship('Path', back_populates='models')

    model_id = Column(Integer, ForeignKey('model.id'), primary_key=True, nullable=False)
    model = relationship('Model', back_populates='paths')
