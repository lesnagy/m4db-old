r'''
Defines some useful global values.
'''

import os
import configparser

config_file_apath = os.path.join(
    os.path.expanduser("~"),
    ".m4db"
)

config_file_aname = os.path.join(
    config_file_apath,
    '.m4db.ini'
)

config = configparser.RawConfigParser()

if os.path.isfile(config_file_aname):
    config.read(config_file_aname)
else:
    config['general-ui'] = {
        'filter-start-point': 'geometry'
    }

    config['logging'] = {
        'level': 'debug',
        'output': 'm4db.log',
        'format': r'[%(pathname)130s:%(lineno)s - %(funcName)50s() ] %(message)s'
    }

    config['geometry'] = {
        'size-unit': 'nm',
        'size-convention': 'ESVD'
    }

    config['database'] = {
        'url-template': 'postgresql://{user:}@{host:}/{db:}',
        'user': 'lnagy2',
        'host': 'totaig',
        'db': 'mmmm'
    }

    config['rest'] = {
        'base-url': 'localhost:6543'
    }

    config['external'] = {
        'mm-solver': 'merrill'
    }

    if not os.path.isdir(config_file_apath):
        os.mkdir(config_file_apath)

    if not os.path.exists(config_file_aname):
        with open(config_file_aname, 'w') as fout:
            config.write(fout)

