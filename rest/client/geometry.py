r'''
Python implementations of some useful utility functions to communicate with the
REST server.

'''
import os
import datetime
import dateutil.parser
import json
import requests
import base64
import zlib

from urllib.parse import urljoin

from preferences import config_file_apath
from preferences import config

from log import logger

def geometry_collection():
    r'''
    Retrieve all geometry data in the database.

    Args:
        none

    Returns:
        a JSON object containing geometry information, if no information was
        found None is returned.

    Raises:
        none

    '''
    ntries = int(config['rest']['max-tries'])

    for t in range(ntries):
        try:
            url = urljoin(
                config['rest']['base-url'],
                'geometry'
            )
            response = requests.get(url)
            logger.debug("Geometry rest url: {} data retrieved".format(repr(url)))
            return response.json()
        except Exception as e:
            logger.debug(e)
            logger.debug("Failed geometry rest: {}".format(t+1))

    return None


def geometry_metadata(unique_id):
    r'''
    Retrieve the metadata of the geometry with the given unique id.

    Args:
        unique_id: the unique id of the geometry information to retrieve

    Returns:
        A JSON object containing the requested geometry name, or None if the
        geometry requested could not be found.

    Raises:
        none

    '''
    ntries = int(config['rest']['max-tries'])
    for t in range(ntries):
        try:
            url = urljoin(
                config['rest']['base-url'],
                'geometry/{}'.format(unique_id)
            )
            response = requests.get(url)
            logger.debug("Geometry rest url: {} data retrieved".format(repr(url)))
            return response.json()
        except Exception as e:
            logger.debug(e)
            logger.debug("Failed geometry rest: {}".format(t+1))

    return None


def geometry_patran_mesh(unique_id, force=False):
    r'''
    TODO: [Les] check this!
    Retreive the mesh data for the geometry.

    Args:
        unique_id: the unique id of the geometry for which we would like to
                   retrieve the patran (mesh) data.

        force:     a boolean that overrides caching behaviour, if set to true
                   this function will always pull down the version of the
                   geometry in the database and will not perform a cache check.

    Returns:
        True if the routine was successful in downloading the file, otherwise
        False.


    Raises:
        None

    '''
    cache_dir = os.path.join(
        config_file_apath,
        'geometry',
        unique_id
    )

    cache_mesh = os.path.join(
        cache_dir,
        'mesh.pat'
    )

    if not force:
        logger.debug("Checking cache")
        server_retreive = True

        if not os.path.isdir(cache_dir):
            # If cache dir does not exist, create it and try again
            logger.debug("Could not find {}, create and force download".format(cache_dir))
            os.makedirs(cache_dir)
            return geometry_patran_mesh(unique_id, force=True)
        elif not os.path.isfile(cache_mesh):
            # If the file does not exist, force download
            logger.debug("Could not find {}, force download".format(cache_mesh))
            return geometry_patran_mesh(unique_id, force=True)
        else:
            # If cache dir exists & the file exists, get the timestamp & check
            timestamp = datetime.datetime.fromtimestamp(
                os.path.getmtime(
                    cache_mesh
                )
            )

            logger.debug("Checking timestamp of cached/server file")
            metadata = geometry_metadata(unique_id)
            if len(metadata) > 1:
                last_modified = dateutil.parser.parse(metadata[0]['last_modified'])
                if timestamp < last_modified:
                    logger.debug("Cached file is older than server file, force download")
                    geometry_patran_mesh(unique_id, force=True)
    else:
        logger.debug("Downloading geometry {}".format(unique_id))

        ntries = int(config['rest']['max-tries'])

        for t in range(ntries):
            try:
                url = urljoin(
                    config['rest']['base-url'],
                    'geometry/patran/{}'.format(unique_id)
                )
                response = requests.get(url)

                logger.debug("Geometry rest url: {} data retrieved".format(repr(url)))
                json_response = response.json()

                logger.debug("Unpacking data")
                data = zlib.decompress(
                    base64.b64decode(
                        json_response[0]['data']
                    )
                )

                logger.debug("Writing data")
                with open(cache_mesh, 'w') as fout:
                    encoding = config['rest']['encoding']
                    fout.write(data.decode(encoding))
                return True
            except Exception as e:
                logger.debug(e)
                logger.debug("Failed geometry rest: {}".format(t+1))

    # If we got here we failed to donwload the file
    return False

