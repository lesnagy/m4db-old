import os
import zlib
import base64

import falcon
import mimetypes

from db import geometry

import json

import orm

from database import get_session

from log import logger

from preferences import config


def geometry_to_dictionary(geometry):
    r'''
    Returns a python dictionary representation of a geometry object.

    Args:
        geometry: orm object representing geometry information

    Returns:
        a python dictionary representation of the input geometry

    Raises:
        none

    '''
    return {
        'id': geometry.id,
        'unique_id': geometry.unique_id,
        'name': geometry.name,
        'size': geometry.size,
        'element_size': geometry.element_size,
        'description': geometry.description,
        'nelements': geometry.nelements,
        'nvertices': geometry.nvertices,
        'volume_total': geometry.volume_total,
        'element_volume_average': geometry.element_volume_average,
        'element_volume_std': geometry.element_volume_std,
        'element_distance_avg': geometry.element_distance_average,
        'element_distance_std': geometry.element_distance_std,
        'has_patran': geometry.has_patran,
        'has_exodus': geometry.has_exodus,
        'has_mesh_gen_script': geometry.has_mesh_gen_script,
        'last_modified': str(geometry.last_modified),
        'created': str(geometry.created),
        'size_unit': geometry.size_unit.symbol,
        'element_size_unit': geometry.element_size_unit,
        'size_convention': geometry.size_convention.symbol,
        'intrinsic_unit': geometry.mesh_intrinsic_unit.symbol
    }


class GeometryCollection(object):
    r'''
    Class to retreive all geometry metadata and post it in JSON format.

    Args:
        none

    Returns:
        none (output posted via http user reponse)

    Raises:
        none

    '''
    def on_get(self, req, resp):
        session = get_session()

        resp.status = falcon.HTTP_200

        query_result = geometry.get(session)

        output_list = []
        for geom in query_result:
            output_list.append(geometry_to_dictionary(geom))

        resp.body = json.dumps(output_list)


class GeometryMetadata(object):
    r'''
    Class to retreive a single geomery's metadata in JSON format.

    Args:
        none

    Returns:
        none (output posted via http user reponse)

    Raises:
        none

    '''
    def on_get(self, req, resp, unique_id):
        session = get_session()

        resp.status = falcon.HTTP_200

        query_result = session.query(orm.Geometry).\
                filter(orm.Geometry.unique_id == unique_id).\
                all()
        if len(query_result) == 0:
            raise IOError("No geometry found for unique id {}".format(unique_id))

        output_list = []
        for geom in query_result:
            output_list.append(geometry_to_dictionary(geom))

        resp.body = json.dumps(output_list)


class GeometryPatranMesh(object):
    r'''
    Class to retrieve the patran mesh for a given object.

    please note that:
        the patran file is first zipped using the python zlib library [1] and
        then base64 encoded before being marshalled to JSON and posted via http
        in response to the user.

        There are known incompatibilities between the Python module and
        versions of the zlib library earlier than 1.1.3; 1.1.3 has a security
        vulnerability, so we recommend using 1.1.4 or later. [1]

        [1] https://docs.python.org/2/library/zlib.html

    Args:
        none

    Returns:
        none (output posted via http user reponse)

    Raises:
        none

    '''
    def on_get(self, req, resp, unique_id):
        file_root = config['database']['file-root']
        rest_encoding = config['rest']['encoding']
        mesh_file = os.path.join(
            file_root,
            'geometry',
            unique_id,
            'mesh.pat'
        )
        logger.debug(mesh_file)
        if not os.path.isfile(mesh_file):
            raise IOError("No patran file for {}".format(unique_id))
        with open(mesh_file, 'rb') as fin:
            data = base64.b64encode(
                zlib.compress(
                    fin.read()
                )
            )
            resp.status = falcon.HTTP_200
            resp.body = json.dumps([{
                'unique_id': unique_id,
                'data' : data.decode(rest_encoding)
            }])

