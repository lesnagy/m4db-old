r"""
Definition of services provided by m4db. This file basically lists URL hooks
in to functions that return JSON marshalled data. Please see [1] for more
details.

[1] https://falconframework.org/

"""
import falcon

from log import *

import rest.server.geometry

app = falcon.API()

geometry_collection = rest.server.geometry.GeometryCollection()
geometry_metadata = rest.server.geometry.GeometryMetadata()
geometry_patran_mesh = rest.server.geometry.GeometryPatranMesh()

app.add_route('/geometry', geometry_collection)
app.add_route('/geometry/{unique_id}', geometry_metadata)
app.add_route('/geometry/patran/{unique_id}', geometry_patran_mesh)

