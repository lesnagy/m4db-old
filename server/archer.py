import re
from subprocess import Popen, PIPE

from config import ConfigData
config_data = ConfigData()


def qstat_parser(qstat_output):

    col_job_id = 0
    col_user_name = 1
    con_queue = 2
    col_job_name = 3
    col_session_id = 4
    col_nds = 5
    col_tsk = 6
    col_required_memory = 7
    col_required_time = 8
    col_s = 9
    col_elapsed_time = 10

    qs_lines = qstat_output.splitlines()

    output = []

    regex_table_line = re.compile(r'^[-\s]+$')

    for qs_line in qs_lines:
        values = [x.encode('ascii') for x in qs_line.split()]
        if len(values) == 11 and not regex_table_line.match(qs_line):
            output.append(
                {
                    'job_id': values[col_job_id],
                    'user_name': values[col_user_name],
                    'queue': values[con_queue],
                    'job_name': values[col_job_name],
                    'session_id': values[col_session_id],
                    'nds': values[col_nds],
                    'tsk': values[col_tsk],
                    'required_memory': values[col_required_memory],
                    'required_time': values[col_required_time],
                    's': values[col_s],
                    'elapsed_time': values[col_elapsed_time],
                }
            )
    return output


def get_n_running_jobs():
    command_template = '''ssh -t {user:}@{host:} "qstat -u {user:}"'''
    #command_template = '''ssh -t {user:}@{host:} "qstat"'''
    command = command_template.format(
        user=config_data.compute_resources['archer']['host_user'],
        host=config_data.compute_resources['archer']['host_url'],
    )
    print(command)

    proc = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)

    stdout, stderr = proc.communicate()

    if proc.returncode != 0:
        raise ValueError("\n{}".format(stderr))

    return len(qstat_parser(stdout.decode('ascii')))


def archer():
    r"""
    This is a process specifically for archer. It looks in the unrun_job directory and shedules
    a batch using the python task farm (ptf) module
    :return:
    """
    pass


get_n_running_jobs()
