import os
import re
import time

import server.util as sutil

import multiprocessing as mp

from preferences import config

def is_zip_file(abs_file):

    if not hasattr(is_zip_file, 'regex_zip'):
        is_zip_file.regex_zip = re.compile(r'(.*)\.zip$')

    abs_normed_file = os.path.normpath(abs_file)
    file_name = os.path.split(abs_normed_file)[1]

    if os.path.isfile(abs_file):
        match_zip_file = is_zip_file.regex_zip.match(file_name)
        if match_zip_file:
            return True

    return False


def zip_id(abs_zip_file):

    zip_file = os.path.split(abs_zip_file)[1]

    if not hasattr(zip_id, 'regex_zip'):
        zip_id.regex_zip = re.compile(r'(.*)\.zip$')

    match_zip_file = zip_id.regex_zip.match(zip_file)
    if match_zip_file:
        return match_zip_file.group(1)
    else:
        return None


def daemon_run(rank):
    r"""
    This is the 'usual deamon' i.e. it consists of a pool that 'spins around'
    looking for zip files in the unrun_job directory

    :return: None
    """

    unrun_job_dir = os.path.join(
        config.get('micromag-server', 'file-root'),
        'unrun_job'
    )

    sleep_time = int(config.get('micromag-server', 'sleep-time'))

    while True:
        if rank == 0:
            try:
                # Master rank puts jobs on to the queue
                zip_files = [
                    f for f in [
                        os.path.join(unrun_job_dir, f)
                        for f in os.listdir(unrun_job_dir)
                    ] if is_zip_file(f)
                ]

                unique_ids = [zip_id(f) for f in zip_files]

                for unique_id in unique_ids:
                    # Prepare the job
                    sutil.prepare_job(unique_id)
                    print("Master process has prepared job {}".format(unique_id))

                    # Put the id on to the queue
                    daemon_run.queue.put(unique_id)
                    print("Master process put {} on the queue".format(unique_id))

                time.sleep(sleep_time)
            except Exception as e:
                print("Master process error: {}".format(repr(e)))
                continue
        else:
            try:
                # Slave process executes micromagnetics code
                if daemon_run.queue.empty():
                    # time.sleep(sleep_time)
                    continue
                else:
                    unique_id = daemon_run.queue.get()

                    print(
                        "Rank {rank:} running {id:}".format(
                            rank=rank,
                            id=unique_id
                        )
                    )

                    sutil.execute_job(unique_id)

                    print(
                        "Rank {rank:} finished {id:} ... cleaning up".format(
                            rank=rank,
                            id=unique_id
                        )
                    )

                    sutil.cleanup_job(unique_id)
            except Exception as e:
                print("Rank {rank:} error: {}".format(repr(e)))
                continue


def daemon_init(queue):
    print("Initializing the queue")
    daemon_run.queue = queue


def daemon():
    max_jobs = int(config.get('micromag-server', 'max-jobs'))

    queue = mp.Queue()
    pool = mp.Pool(
        max_jobs,
        daemon_init,
        [queue]
    )
    ranks = range(max_jobs)
    pool.map(daemon_run, ranks)

