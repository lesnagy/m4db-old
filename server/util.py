import shutil
import os

from subprocess import Popen, PIPE

import server.default as default

from preferences import config

def prepare_job(unique_id):
    r"""
    This takes an archive identified by 'unique_id' from the unrun_job_directory and unpacks it in
    to the running_job_directory, it then removes the original archive.
    :param unique_id: the unique id that forms the basis of the archive name.
    :return: None
    """

    archive_type = config.get('micromag-server', 'archive-type')
    unrun_job_dir = os.path.join(
        config.get('micromag-server', 'file-root'),
        'unrun_job'
    )
    running_job_dir = os.path.join(
        config.get('micromag-server', 'file-root'),
        'running_job'
    )

    archive_name_template = '{archive_id:}.{archive_type:}'
    archive_name = archive_name_template.format(
        archive_id=unique_id,
        archive_type=archive_type
    )

    splt = os.path.splitext(archive_name)
    unique_id = splt[0]

    # Make sure that there is no directory already existing (if it exists
    # delete it and start again).
    running_job_id_dir = os.path.join(
        running_job_dir,
        unique_id
    )

    if os.path.isdir(running_job_id_dir):
        shutil.rmtree(running_job_id_dir)


    # Unzip the archive from the urun_job directory in to the running_job directory
    shutil.unpack_archive(
        filename=os.path.join(
            unrun_job_dir,
            archive_name
        ),
        extract_dir=os.path.join(
            running_job_dir,
            unique_id
        ),
        format=archive_type
    )

    # Delete the archive
    os.remove(
        os.path.join(
            unrun_job_dir,
            archive_name
        )
    )


def execute_job(unique_id):
    r'''
    Change to the correct correct sub directory associated with the model in
    running_job_directory

    '''
    running_job_dir = os.path.join(
        config.get('micromag-server', 'file-root'),
        'running_job'
    )
    micromag_executable = config.get('micromag-server', 'micromag')

    os.chdir(
        os.path.join(
            running_job_dir,
            unique_id
        ))

    # Execute micromagnetic program
    command_template = '{command:} {mscript:}'
    command = command_template.format(
        command=micromag_executable,
        mscript='mscript'
    )

    proc = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)

    stdout, stderr = proc.communicate()

    with open('stdout', 'w') as stdout_fout:
        stdout_fout.write("\n{}\n".format(stdout.decode('ascii')))
    with open('stderr', 'w') as stderr_fout:
        stderr_fout.write("\n{}\n".format(stderr.decode('ascii')))

    if proc.returncode != 0:
        open('FAILED').close()


def cleanup_job(unique_id):

    archive_type = config.get('micromag-server', 'archive-type')
    running_job_dir = os.path.join(
        config.get('micromag-server', 'file-root')
        'running_job'
    )
    finished_job_dir = os.path.join(
        config.get('micromag-server', 'file-root')
        'finished_job'
    )

    # Change to the correct correct sub directory associated with the model
    # in running_job_directory
    os.chdir(
        os.path.join(
            running_job_dir,
            unique_id
        )
    )

    # Open up the DELETE_ME file, delete the files therein and the remove DELETE_ME
    if os.path.isfile('DELETE_ME'):
        with open('DELETE_ME', 'r') as fin:
            for line in fin:
                file_name = line.strip()
                if os.path.isfile(file_name):
                    os.remove(file_name)
        os.remove('DELETE_ME')

    # Switch to the running_job directory
    os.chdir(finished_job_dir)

    # Zip the directory associated with our job in to the finnished_job directory
    shutil.make_archive(
        unique_id,
        archive_type,
        os.path.join(
            running_job_dir,
            unique_id
        )
    )

    # Delete the directory associated with the job
    shutil.rmtree(
        os.path.join(
            running_job_dir,
            unique_id
        )
    )
