r'''
A module to parse a collection of tables in 'table format' which looks like

* database_table_name
col val 1, row 1 | col val 2, row 1 | col val 3, row 1
col val 1, row 2 | col val 2, row 2 | col val 3, row 2
col val 1, row 3 | col val 2, row 3 | col val 3, row 3

* another_table_name
col val 1, row 1 | col val 2, row 1 | col val 3, row 1 | col val 4, row 1
col val 1, row 2 | col val 2, row 2 | col val 3, row 2 | col val 4, row 2
col val 1, row 3 | col val 2, row 3 | col val 3, row 3 | col val 4, row 3
col val 1, row 4 | col val 2, row 4 | col val 3, row 4 | col val 4, row 4
col val 1, row 5 | col val 2, row 5 | col val 3, row 5 | col val 4, row 5

'''

import orm

def parse_table(filename):
    r'''
    Parse a file with a collection of tables in the format outlined previously

    Args:
        filename: the name of the file containing table data

    Output:
        a 2D array of length M x N, where M is the number of rows and N is the
        number of columns.

    Raises:
        none
        TODO: [Les] should raise exceptions on data validation errors.
    '''

    current_table = None

    tables = {}

    with open(filename, 'r') as fin:
        for line in fin:
            line = line.strip()
            if len(line) == 0:
                pass
            elif line[0] == '*':
                current_table = line[1:].strip()
                tables[current_table] = []
            elif current_table is not None:
                columns = line.split('|')
                if len(columns) > 0:
                    row = [col.strip() for col in columns]
                    tables[current_table].append(row)

    return tables


def table_of_orm_objects(table):
    orm_table = {}
    for table, tdata in table.items():
        orm_table[table] = []
        if table == 'users':
            for row in tdata:
                orm_table[table].append(
                    orm.DBUser(
                        user_name=row[0],
                        first_name=row[1],
                        initials=row[2],
                        surname=row[3],
                        email=row[4],
                        telephone=row[5]
                    )
                )
        elif table == 'path_calculation':
            for row in tdata:
                orm_table[table].append(
                    orm.PathCalculation(
                        name = row[0],
                        description = row[1]
                    )
                )
        elif table == 'software':
            for row in tdata:
                orm_table[table].append(
                    orm.Software(
                        name=row[0],
                        version=row[1],
                        description=row[2],
                        url=row[3],
                        citation=row[4]
                    )
                )
        elif table == 'running_status':
            for row in tdata:
                orm_table[table].append(
                    orm.RunningStatus(
                        name=row[0],
                        description=row[1]
                    )
                )
        elif table == 'physical_constant':
            for row in tdata:
                print("skldfjldsfj {}".format(row[2]))
                orm_table[table].append(
                    orm.PhysicalConstant(
                       symbol=row[0],
                       name=row[1],
                       value=float(row[2]),
                       unit=row[3]
                    )
                )
        elif table == 'unit':
            for row in tdata:
                orm_table[table].append(
                    orm.Unit(
                        symbol=row[0],
                        name=row[1],
                        power=float(row[2])
                    )
                )
        elif table == 'size_convention':
            for row in tdata:
                orm_table[table].append(
                    orm.SizeConvention(
                        symbol=row[0],
                        description=row[1]
                    )
                )
        elif table == 'anisotropy_form':
            for row in tdata:
                orm_table[table].append(
                    orm.AnisotropyForm(
                        name=row[0],
                        description=row[1]
                    )
                )
        elif table == 'running_location':
            for row in tdata:
                orm_table[table].append(
                    orm.RunningLocation(
                       name=row[0],
                       url=row[1],
                       base_directory=row[2],
                       ssh_key=row[3]
                    )
                )

    return orm_table

