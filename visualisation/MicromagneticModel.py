#
# \file   MicromagneticModel.py
# \author L. Nagy
#
# Copyright: All rights reserved.
#

import vtk
import re
import numpy as np

#from pubsub import pub
from wx.lib.pubsub import pub

class TecplotMicromagneticModel:
    

    def __init__(self, absFileName):
        r'''
        This class will read micromagnetic model data from a tecplot file and
        produce a VTK-exporable model. This class can read both multi-zone and
        single zone tecplot files.

        :param absFileName: the file name of the tecplot file holding 
                            micromagnetic model data.
        '''

        self.absFileName = absFileName

        self.ug = vtk.vtkUnstructuredGrid()
        self.pathSolutions = []
        self.gHMin = None
        self.gHMax = None
        self.gHAvg = None
        self.lut = None
        self.ctf = None
        self.currentZone = 0
        self.nzones = 0
        self.init()

        self.isoH = self.gHAvg

        #######################################################################
        # Arrow source format.
        #######################################################################

        self.arrowSource = vtk.vtkArrowSource()
        self.arrowSource.SetShaftResolution(30)
        self.arrowSource.SetTipResolution(50)

        self.transform = vtk.vtkTransform()
        self.transform.Translate([-0.5, 0.0, 0.0])

        self.transPolyData = vtk.vtkTransformPolyDataFilter()
        self.transPolyData.SetTransform(self.transform)
        self.transPolyData.SetInputConnection(self.arrowSource.GetOutputPort())

        ####
        # Vectors
        ####

        # Create the glyphs
        self.glyph = vtk.vtkGlyph3D()
        self.glyph.SetSourceConnection(self.transPolyData.GetOutputPort())
        self.glyph.SetInputData(self.ug) 
        self.glyph.ScalingOn()
        self.glyph.SetScaleModeToScaleByVector()
        self.glyph.SetVectorModeToUseVector()
        self.glyph.SetColorModeToColorByScalar()
        self.glyph.OrientOn()
        self.glyph.SetScaleFactor(0.020)
        self.glyph.Update()

        self.fieldMapper = vtk.vtkPolyDataMapper()
        self.fieldMapper.SetInputConnection(self.glyph.GetOutputPort())
        self.fieldMapper.ScalarVisibilityOn()
        self.fieldMapper.SetLookupTable(self.lut)
        self.fieldMapper.SetScalarRange(self.gHMin, self.gHMax)
        self.fieldMapper.Update()

        self.fieldActor = vtk.vtkActor()
        self.fieldActor.SetMapper(self.fieldMapper)

        ####
        # Helicity
        ####

        self.isosurface = vtk.vtkContourGrid()
        self.isosurface.SetInputData(self.ug)
        self.isosurface.SetValue(0, self.isoH)

        self.isosurfaceMapper = vtk.vtkPolyDataMapper()
        self.isosurfaceMapper.ScalarVisibilityOff()
        self.isosurfaceMapper.SetInputData(self.isosurface.GetOutput())
        self.isosurfaceMapper.Update()

        self.isosurfaceActor = vtk.vtkActor()
        self.isosurfaceActor.SetMapper(self.isosurfaceMapper)
        self.isosurfaceActor.GetProperty().SetColor(1.0, 1.0, 1.0)
        self.isosurfaceActor.Modified()

        ####
        # Geometry
        ####

        self.geometrySurface = vtk.vtkDataSetSurfaceFilter()
        self.geometrySurface.SetInputData(self.ug)
        self.geometrySurface.Update()

        self.geometryMapper = vtk.vtkDataSetMapper()
        self.geometryMapper.SetInputData(self.geometrySurface.GetOutput())
        self.geometryMapper.ScalarVisibilityOff()
        self.geometryMapper.Update()

        self.geometryActor = vtk.vtkActor()
        self.geometryActor.SetMapper(self.geometryMapper)
        self.geometryActor.Modified()

        ####
        # Wireframe
        ####

        self.wireframeActor = vtk.vtkActor()
        self.wireframeActor.SetMapper(self.geometryMapper)
        self.wireframeActor.GetProperty().SetRepresentationToWireframe()
        self.wireframeActor.GetProperty().SetAmbient(1)
        self.wireframeActor.GetProperty().SetDiffuse(0)
        self.wireframeActor.Modified()

        # Display the first point in the path by default.
        self.setCurrentZone(0)


    def init(self):
        r'''
        PRIVATE: function to initalize micromagnetic model data. This function
        basically reads all the data in from a tecplot file and creates a 
        vtk unstrcuted grid object containing the data.
        '''
          
        strFloat = r'[-+]?\d*\.?\d+([eE][-+]?\d+)?'
    
        regexZone = re.compile(r'ZONE T="\s*(\d+)\s*"N=\s*(\d+)\s*,E=\s*(\d+)')

        regexVertMag = re.compile(r'({})\s+({})\s+({})\s+({})\s+({})\s+({})'.format(
            strFloat, strFloat, strFloat, strFloat, strFloat, strFloat))

        regexConn = re.compile(r'(\d+)\s+(\d+)\s+(\d+)\s+(\d+)')

        regexMag = re.compile(r'({})\s+({})\s+({})'.format(
            strFloat, strFloat, strFloat))
    
        # Calculate the number of zones.
        self.nzones = 0
        with open(self.absFileName) as fin:
            for line in fin:
                matchZone = regexZone.search(line)
                if matchZone:
                    self.nzones = self.nzones+1
        mag  = None
        vert = None
        conn = None
        with open(self.absFileName) as fin:
            currentZone = -1
            nvert = -1
            nelem = -1
            magCounter  = 0
            vertCounter = 0
            connCounter = 0
            for line in fin:
                matchZone = regexZone.search(line)
                if matchZone:
                    currentZone = int(matchZone.group(1))
                    nvert       = int(matchZone.group(2))
                    nelem       = int(matchZone.group(3))
    
                    magCounter  = 0
                    vertCounter = 0
                    connCounter = 0
    
                    if currentZone == 1:
                        # Initialize arrays
                        mag  = np.zeros((self.nzones, nvert, 3))
                        vert = np.zeros((nvert, 3))
                        conn = np.zeros((nelem, 4), dtype=np.int)
    
                    continue
                
                if currentZone == 1:
                    matchVertMag = regexVertMag.search(line)
                    if matchVertMag:
                        vert[vertCounter][0] = float(matchVertMag.group(1))
                        vert[vertCounter][1] = float(matchVertMag.group(3))
                        vert[vertCounter][2] = float(matchVertMag.group(5))
                        
                        mag[currentZone-1][magCounter][0] = float(matchVertMag.group(7))
                        mag[currentZone-1][magCounter][1] = float(matchVertMag.group(9))
                        mag[currentZone-1][magCounter][2] = float(matchVertMag.group(11))
    
                        vertCounter = vertCounter + 1
                        magCounter  = magCounter + 1
                        continue
    
                    matchConn = regexConn.search(line)
                    if matchConn:
                        conn[connCounter][0] = int(matchConn.group(1)) - 1
                        conn[connCounter][1] = int(matchConn.group(2)) - 1
                        conn[connCounter][2] = int(matchConn.group(3)) - 1 
                        conn[connCounter][3] = int(matchConn.group(4)) - 1
                        connCounter = connCounter + 1
                        continue
                elif currentZone > 1:
                    matchMag = regexMag.search(line)
                    if matchMag:
                        mag[currentZone-1][magCounter][0] = float(matchMag.group(1))
                        mag[currentZone-1][magCounter][1] = float(matchMag.group(3))
                        mag[currentZone-1][magCounter][2] = float(matchMag.group(5))
    
                        magCounter = magCounter + 1

        # Vertices.
        points = vtk.vtkPoints()
        points.SetNumberOfPoints(vert.shape[0])
        for i in range(vert.shape[0]):
            points.SetPoint(i, vert[i,0], vert[i,1], vert[i,2])
        self.ug.SetPoints(points)

        # Connectivity
        for i in range(conn.shape[0]):
            element = vtk.vtkIdList()

            element.InsertNextId(conn[i][0])
            element.InsertNextId(conn[i][1])
            element.InsertNextId(conn[i][2])
            element.InsertNextId(conn[i][3])

            self.ug.InsertNextCell(vtk.VTK_TETRA, element)

        # Magnetizations in all the zones.
        for zone in range(mag.shape[0]):
            field = vtk.vtkDoubleArray()
            field.SetName(self.pathIdNameM(zone))
            field.SetNumberOfComponents(3)
            field.SetNumberOfTuples(mag.shape[1])

            self.pathSolutions.append(self.pathIdNameM(zone))

            for i in range(mag.shape[1]):
                field.SetTuple3(i, mag[zone][i][0], 
                                   mag[zone][i][1], 
                                   mag[zone][i][2])

            self.ug.GetPointData().AddArray(field)

        # Helicities in all zones.
        self.gHMin =  1E100
        self.gHMax = -1E100
        self.gHAvg = 0.0
        for zone in range(mag.shape[0]):
            vorticity = vtk.vtkGradientFilter()
            vorticity.ComputeVorticityOn()
            vorticity.SetInputArrayToProcess(0, 0, 0, 0, self.pathIdNameM(zone))
            vorticity.SetResultArrayName('V')
            vorticity.SetInputData(self.ug)
            vorticity.Update()

            helicity = vtk.vtkArrayCalculator()
            helicity.SetAttributeModeToUsePointData()
            helicity.AddVectorArrayName(self.pathIdNameM(zone))
            helicity.AddVectorArrayName('V')
            helicity.SetResultArrayName('H')
            helicity.SetFunction('{}.V'.format(self.pathIdNameM(zone)))
            helicity.SetInputData(vorticity.GetOutput())
            helicity.Update()

            helicityData = helicity.GetOutput().GetPointData().GetArray('H')

            helicityField = vtk.vtkDoubleArray()
            helicityField.SetName(self.pathIdNameH(zone))
            helicityField.SetNumberOfComponents(1)
            helicityField.SetNumberOfTuples(helicityData.GetNumberOfTuples())

            for i in range(helicityField.GetNumberOfTuples()):

                # Minimum/maximum
                if helicityData.GetValue(i) < self.gHMin:
                    self.gHMin = helicityData.GetValue(i)
                if helicityData.GetValue(i) > self.gHMax:
                    self.gHMax = helicityData.GetValue(i)

                # Add helicity to field
                helicityField.SetValue(i, helicityData.GetValue(i))

                # Accumulate helicity values in average
                self.gHAvg += helicityData.GetValue(i)

            self.ug.GetPointData().AddArray(helicityField)

        self.gHAvg /= (float(mag.shape[0])*float(mag.shape[1])) 

        # LUTs
        self.lut = vtk.vtkLookupTable()
        self.ctf = vtk.vtkColorTransferFunction()
        nlut = 1000
        dHelicity = abs(self.gHMax-self.gHMin)/float(nlut)

        self.lut.SetNumberOfTableValues(nlut)

        self.ctf.AddRGBSegment(
                (self.gHMin)               , 0.0, 0.0, 1.0,
                (self.gHMin+self.gHMax)/2.0, 1.0, 1.0, 1.0)
        self.ctf.AddRGBSegment(
                (self.gHMin+self.gHMax)/2.0, 1.0, 1.0, 1.0, 
                (self.gHMax)               , 1.0, 0.0, 0.0)

        for i in range(nlut):
            scalar = self.gHMin + float(i)*dHelicity
            cc = self.ctf.GetColor(scalar)
            self.lut.SetTableValue(i, cc[0], cc[1], cc[2])


    def pathIdNameM(self, pathId):
        return 'm_{}'.format(pathId)


    def pathIdNameH(self, pathId):
        return 'h_{}'.format(pathId)


    def pathId(self, pathId):
        return int(pathId.split('_')[1])


    def getFieldActor(self):
        return self.fieldActor


    def getIsosurfaceActor(self):
        return self.isosurfaceActor


    def getGeometryActor(self):
        return self.geometryActor


    def getWireframeActor(self):
        return self.wireframeActor


    def setCurrentZone(self, zone):
        self.currentZone = zone
        self.ug.GetPointData().SetActiveVectors(self.pathIdNameM(self.currentZone))
        self.ug.GetPointData().SetActiveScalars(self.pathIdNameH(self.currentZone))
        self.geometryMapper.Update()
        self.isosurfaceMapper.Update()
        self.fieldMapper.Update()


    def getCurrentZone(self):
        return self.currentZone


    def getNZones(self):
        return self.nzones
    

    def nextModel(self):
        if self.currentZone+1 < self.nzones:
            self.setCurrentZone(self.currentZone+1)
        pub.sendMessage('micromagnetic.model.next', value=self.currentZone, oneOffset=True) 


    def previousModel(self):
        if -1 < self.currentZone-1:
            self.setCurrentZone(self.currentZone-1)
        pub.sendMessage('micromagnetic.model.previous', value=self.currentZone, oneOffset=True)


    def jumpToModel(self, index):
        self.setCurrentZone(index)
        pub.sendMessage('micromagnetic.model.jump', value=self.currentZone, oneOffset=True) 
        
    
